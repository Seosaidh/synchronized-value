#include <mutex>
#if __cplusplus >= 201402L
  #include <shared_mutex>
#endif
#include <chrono>
#include <iostream>
#include <string>
#include <thread>
#include <condition_variable>
#include "../seo_sync_val.hpp"

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

struct Foo {
  int x;
  double y;
};


TEST_CASE("Simple Instantiation", "[construction]")
{
  seo::SyncVal<std::mutex, std::string>                 synced_val{{}};
  seo::SyncVal<std::recursive_mutex, std::string>       recursive_val{{}};
  seo::SyncVal<std::timed_mutex, std::string>           timed_val{{}};
  seo::SyncVal<std::recursive_timed_mutex, std::string> recurs_timed_val{{}};
#if __cplusplus >= 201703L
  seo::SyncVal<std::shared_mutex, std::string> shared_val{{}};
#endif
#if __cplusplus >= 201402L
  seo::SyncVal<std::shared_timed_mutex, std::string> shared_timed_val{{}};
#endif
}

TEST_CASE("Move-Assignment with SyncValGuard", "[assignment], [operators]")
{
  seo::SyncVal<std::mutex, std::string>       synced_string{""};
  seo::SyncVal<std::timed_mutex, std::string> synced_string2{"Hello"};
  seo::SyncVal<std::mutex, std::string>       synced_string3{"Hello2"};

  CHECK(*synced_string.lock() == "");
  CHECK(*synced_string2.lock() == "Hello");
  CHECK(*synced_string3.lock() == "Hello2");

  synced_string.lock() = synced_string3.lock();

  CHECK(*synced_string.lock() == "");
  CHECK(*synced_string2.lock() == "Hello");
  CHECK(*synced_string3.lock() == "Hello2");

{
  auto locked_val = synced_string.lock();

  *locked_val = "Word!";
  
  CHECK(*locked_val == "Word!");

  locked_val = synced_string3.lock();

  CHECK(*locked_val == "Hello2");

  *locked_val = "B";
}

  CHECK(*synced_string.lock() == "Word!");
  CHECK(*synced_string2.lock() == "Hello");
  CHECK(*synced_string3.lock() == "B");

}

TEST_CASE("Multi-lock", "[deadlock]")
{
  seo::SyncVal<std::mutex, int>  synced_int{1};
  seo::SyncVal<std::mutex, long> synced_int2{2};

  std::thread t1 = std::thread{[&](){
    for (unsigned int i = 0; i < 10000; i++) {
      auto rtn_val = seo::lock(synced_int, synced_int2);

      rtn_val.first.get()++;
      rtn_val.second.get()++;
    }
  }};

  std::thread t2 = std::thread{[&](){
    for (unsigned int i = 0; i < 10000; i++) {
      auto rtn_val = seo::lock(synced_int2, synced_int);

      rtn_val.first.get()--;
      rtn_val.second.get()--;
    }
  }};

  t1.join();
  t2.join();

  {
    auto rtn_val = seo::lock(synced_int2, synced_int);

    CHECK(rtn_val.first.get() == 2);
    CHECK(rtn_val.second.get() == 1);
  }

  {
    auto rtn_val = seo::lock(synced_int, synced_int2);

    CHECK(rtn_val.first.get() == 1);
    CHECK(rtn_val.second.get() == 2);
  }
}

TEST_CASE("Tri-lock", "[deadlock]")
{
  seo::SyncVal<std::mutex, int>  synced_int{1};
  seo::SyncVal<std::mutex, long> synced_int2{2};
  seo::SyncVal<std::timed_mutex, long> synced_int3{3};

  std::thread t1 = std::thread{[&](){
    for (unsigned int i = 0; i < 10000; i++) {
      auto rtn_val = seo::lock(synced_int, synced_int2, synced_int3);

      std::get<0>(rtn_val).get()++;
      std::get<1>(rtn_val).get()++;
      std::get<2>(rtn_val).get()++;
    }
  }};

  std::thread t2 = std::thread{[&](){
    for (unsigned int i = 0; i < 10000; i++) {
      auto rtn_val = seo::lock(synced_int3, synced_int, synced_int2);

      std::get<0>(rtn_val).get()--;
      std::get<1>(rtn_val).get()--;
      std::get<2>(rtn_val).get()--;
    }
  }};

  t1.join();
  t2.join();

  {
    auto rtn_val = seo::lock(synced_int2, synced_int, synced_int3);

    CHECK(std::get<0>(rtn_val).get() == 2);
    CHECK(std::get<1>(rtn_val).get() == 1);
    CHECK(std::get<2>(rtn_val).get() == 3);
  }

  {
    auto rtn_val = seo::lock(synced_int3, synced_int, synced_int2);

    CHECK(std::get<0>(rtn_val).get() == 3);
    CHECK(std::get<1>(rtn_val).get() == 1);
    CHECK(std::get<2>(rtn_val).get() == 2);
  }
}

TEST_CASE("Tri-try-lock", "[deadlock], [!mayfail]")
{
  seo::SyncVal<std::mutex, int>  synced_int{1};
  seo::SyncVal<std::mutex, long> synced_int2{2};
  seo::SyncVal<std::timed_mutex, long> synced_int3{3};

  std::thread t1 = std::thread{[&](){
    for (unsigned int i = 0; i < 10000; i++) {
      auto rtn_val = seo::try_lock(synced_int, synced_int2, synced_int3);
      if (std::get<0>(rtn_val).has_value()) {
      std::get<0>(rtn_val).value().get()++;
      std::get<1>(rtn_val).value().get()++;
      std::get<2>(rtn_val).value().get()++;
      }
    }
  }};

  std::thread t2 = std::thread{[&](){
    for (unsigned int i = 0; i < 10000; i++) {
      auto rtn_val = seo::try_lock(synced_int3, synced_int, synced_int2);

      if (std::get<0>(rtn_val).has_value()) {
      std::get<0>(rtn_val).value().get()--;
      std::get<1>(rtn_val).value().get()--;
      std::get<2>(rtn_val).value().get()--;
      }
    }
  }};

  t1.join();
  t2.join();

  {
    auto rtn_val = seo::try_lock(synced_int2, synced_int, synced_int3);

    if (std::get<0>(rtn_val).has_value()) {
      CHECK(std::get<0>(rtn_val).value().get() == 2);
      CHECK(std::get<1>(rtn_val).value().get() == 1);
      CHECK(std::get<2>(rtn_val).value().get() == 3);
    }
  }

  {
    auto rtn_val = seo::try_lock(synced_int3, synced_int, synced_int2);

    if (std::get<0>(rtn_val).has_value()) {
      CHECK(std::get<0>(rtn_val).value().get() == 3);
      CHECK(std::get<1>(rtn_val).value().get() == 1);
      CHECK(std::get<2>(rtn_val).value().get() == 2);
    }
  }
}

TEST_CASE("Multi-try_lock", "[deadlock], [!mayfail]")
{
  seo::SyncVal<std::mutex, int>  synced_int{1};
  seo::SyncVal<std::timed_mutex, long> synced_int2{2};

  std::thread t1 = std::thread{[&](){
    for (unsigned int i = 0; i < 10000; i++) {
      auto rtn_val = seo::try_lock(synced_int, synced_int2);

      if (rtn_val.first.has_value()) {
        rtn_val.first.value().get()++;
      }

      if (rtn_val.second.has_value()) {
        rtn_val.second.value().get()++;
      }
    }
  }};

  std::thread t2 = std::thread{[&](){
    for (unsigned int i = 0; i < 10000; i++) {
      auto rtn_val = seo::try_lock(synced_int2, synced_int);

      if (rtn_val.first.has_value()) {
        rtn_val.first.value().get()--;
      }

      if (rtn_val.second.has_value()) {
        rtn_val.second.value().get()--;
      }
    }
  }};

  t1.join();
  t2.join();

  auto rtn_val = seo::try_lock(synced_int2, synced_int);

  if (rtn_val.first.has_value()) {
        CHECK(rtn_val.first.value().get() == 2);
      }

      if (rtn_val.second.has_value()) {
        CHECK(rtn_val.second.value().get() == 1);
      }
}

TEST_CASE("Condition Variable Wait", "[condition]") {
  seo::SyncVal<std::mutex, int> synced_int{1};
  std::condition_variable cv;

  auto t1 = std::thread{[&](){
    auto locked_val = synced_int.lock();
 
      locked_val.wait(cv);
      CHECK(*locked_val == 100);
      locked_val.get() = 200;
    
  }};

  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  {
    bool set_val = false;

    while(!set_val) {
      auto locked_val = synced_int.lock();

        locked_val.get() = 100;
        set_val = true;
        cv.notify_one();
      
    }
  }

  t1.join();

  auto val = synced_int.lock();

  CHECK(*val == 200);
}

TEST_CASE("Condition Variable Wait With Predicate", "[condition]") {
  seo::SyncVal<std::mutex, int> synced_int{1};
  std::condition_variable cv;

  auto t1 = std::thread{[&](){
    auto locked_val = synced_int.lock();

      locked_val.wait(cv, [&](){return *locked_val == 100;});
      CHECK(*locked_val == 100);
      locked_val.get() = 200;
    
  }};

  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  {
    bool set_val = false;

    while(!set_val) {
      auto locked_val = synced_int.lock();

        locked_val.get() = 100;
        set_val = true;
        cv.notify_one();
      
    }
  }

  t1.join();

  auto val = synced_int.lock();

  CHECK(*val == 200);
}

TEST_CASE("Condition Variable Wait_for", "[condition]") {
  seo::SyncVal<std::mutex, int> synced_int{1};
  std::condition_variable cv;

  auto t1 = std::thread{[&](){
    auto locked_val = synced_int.lock();

      if(locked_val.wait_for(cv, std::chrono::milliseconds(200)) == std::cv_status::no_timeout) {
      CHECK(*locked_val == 100);
        locked_val.get() = 200;
      } else {
        locked_val.get() = 300;
      }
    
  }};

  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  {
    bool set_val = false;

    while(!set_val) {
      auto locked_val = synced_int.lock();

  
        locked_val.get() = 100;
        set_val = true;
        cv.notify_one();
      
    }
  }

  t1.join();

  auto val = synced_int.lock();

  CHECK(*val == 100);
}

TEST_CASE("Condition Variable Wait_for With Predicate", "[condition]") {
  seo::SyncVal<std::mutex, int> synced_int{1};
  std::condition_variable cv;

  auto t1 = std::thread{[&](){
    auto locked_val = synced_int.lock();


      if(locked_val.wait_for(cv, std::chrono::milliseconds(200), [&](){return *locked_val == 100;}) == true) {
      CHECK(*locked_val == 100);
        locked_val.get() = 200;
      } else {
        locked_val.get() = 300;
      }
    
  }};

  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  {
    bool set_val = false;

    while(!set_val) {
      auto locked_val = synced_int.lock();

        locked_val.get() = 100;
        set_val = true;
        cv.notify_one();
      
    }
  }

  t1.join();

  auto val = synced_int.lock();

  CHECK(*val == 100);
}

TEST_CASE("Condition Variable Wait_until", "[condition]") {
  seo::SyncVal<std::mutex, int> synced_int{1};
  std::condition_variable cv;

  auto t1 = std::thread{[&](){
    auto locked_val = synced_int.lock();

      if(locked_val.wait_until(cv, std::chrono::steady_clock::now() + std::chrono::milliseconds(200)) == std::cv_status::no_timeout) {
      CHECK(*locked_val == 100);
        locked_val.get() = 200;
      } else {
        locked_val.get() = 300;
      }
    
  }};

  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  {
    bool set_val = false;

    while(!set_val) {
      auto locked_val = synced_int.lock();

        locked_val.get() = 100;
        set_val = true;
        cv.notify_one();
      
    }
  }

  t1.join();

  auto val = synced_int.lock();

  CHECK(*val == 100);
}

TEST_CASE("Condition Variable Wait_until With Predicate", "[condition]") {
  seo::SyncVal<std::mutex, int> synced_int{1};
  std::condition_variable cv;

  auto t1 = std::thread{[&](){
    auto locked_val = synced_int.lock();

      if(locked_val.wait_until(cv, std::chrono::steady_clock::now() + std::chrono::milliseconds(200), [&](){return *locked_val == 100;}) == true) {
      CHECK(*locked_val == 100);
        locked_val.get() = 200;
      } else {
        locked_val.get() = 300;
      }
    
  }};

  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  {
    bool set_val = false;

    while(!set_val) {
      auto locked_val = synced_int.lock();

        locked_val.get() = 100;
        set_val = true;
        cv.notify_one();
      
    }
  }

  t1.join();

  auto val = synced_int.lock();

  CHECK(*val == 100);
}

TEST_CASE("Condition Variable Any Wait", "[condition]") {
  seo::SyncVal<std::timed_mutex, int> synced_int{1};
  std::condition_variable_any cv;

  auto t1 = std::thread{[&](){
    auto locked_val = synced_int.lock();

      locked_val.wait(cv);
      CHECK(*locked_val == 100);
      locked_val.get() = 200;
    
  }};

  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  {
    bool set_val = false;

    while(!set_val) {
      auto locked_val = synced_int.lock();

        locked_val.get() = 100;
        set_val = true;
        cv.notify_one();
      
    }
  }

  t1.join();

  auto val = synced_int.lock();

  CHECK(*val == 200);
}

TEST_CASE("Condition Variable Any Wait With Predicate", "[condition]") {
  seo::SyncVal<std::recursive_timed_mutex, int> synced_int{1};
  std::condition_variable_any cv;

  auto t1 = std::thread{[&](){
    auto locked_val = synced_int.lock();

      locked_val.wait(cv, [&](){return *locked_val == 100;});
      CHECK(*locked_val == 100);
      locked_val.get() = 200;
    
  }};

  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  {
    bool set_val = false;

    while(!set_val) {
      auto locked_val = synced_int.lock();

     
        locked_val.get() = 100;
        set_val = true;
        cv.notify_one();
      
    }
  }

  t1.join();

  auto val = synced_int.lock();

  CHECK(*val == 200);
}

TEST_CASE("Condition Variable Any Wait_for", "[condition]") {
  seo::SyncVal<std::timed_mutex, int> synced_int{1};
  std::condition_variable_any cv;

  auto t1 = std::thread{[&](){
    auto locked_val = synced_int.lock();

      if(locked_val.wait_for(cv, std::chrono::milliseconds(200)) == std::cv_status::no_timeout) {
      CHECK(*locked_val == 100);
        locked_val.get() = 200;
      } else {
        locked_val.get() = 300;
      }
    
  }};

  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  {
    bool set_val = false;

    while(!set_val) {
      auto locked_val = synced_int.lock();

        locked_val.get() = 100;
        set_val = true;
        cv.notify_one();
      
    }
  }

  t1.join();

  auto val = synced_int.lock();

  CHECK(*val == 100);
}

TEST_CASE("Condition Variable Any Wait_for With Predicate", "[condition]") {
  seo::SyncVal<std::recursive_mutex, int> synced_int{1};
  std::condition_variable_any cv;

  auto t1 = std::thread{[&](){
    auto locked_val = synced_int.lock();
  
      if(locked_val.wait_for(cv, std::chrono::milliseconds(200), [&](){return *locked_val == 100;}) == true) {
      CHECK(*locked_val == 100);
        locked_val.get() = 200;
      } else {
        locked_val.get() = 300;
      }
    
  }};

  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  {
    bool set_val = false;

    while(!set_val) {
      auto locked_val = synced_int.lock();

        locked_val.get() = 100;
        set_val = true;
        cv.notify_one();
      
    }
  }

  t1.join();

  auto val = synced_int.lock();

  CHECK(*val == 100);
}

TEST_CASE("Condition Variable Any Wait_until", "[condition]") {
  seo::SyncVal<std::timed_mutex, int> synced_int{1};
  std::condition_variable_any cv;

  auto t1 = std::thread{[&](){
    auto locked_val = synced_int.lock();

      if(locked_val.wait_until(cv, std::chrono::steady_clock::now() + std::chrono::milliseconds(200)) == std::cv_status::no_timeout) {
      CHECK(*locked_val == 100);
        locked_val.get() = 200;
      } else {
        locked_val.get() = 300;
      }
    
  }};

  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  {
    bool set_val = false;

    while(!set_val) {
      auto locked_val = synced_int.lock();

        locked_val.get() = 100;
        set_val = true;
        cv.notify_one();
      
    }
  }

  t1.join();

  auto val = synced_int.lock();

  CHECK(*val == 100);
}

TEST_CASE("Condition Variable Any Wait_until With Predicate", "[condition]") {
  seo::SyncVal<std::timed_mutex, int> synced_int{1};
  std::condition_variable_any cv;

  auto t1 = std::thread{[&](){
    auto locked_val = synced_int.lock();
  
      if(locked_val.wait_until(cv, std::chrono::steady_clock::now() + std::chrono::milliseconds(200), [&](){return *locked_val == 100;}) == true) {
      CHECK(*locked_val == 100);
        locked_val.get() = 200;
      } else {
        locked_val.get() = 300;
      }
    
  }};

  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  {
    bool set_val = false;

    while(!set_val) {
      auto locked_val = synced_int.lock();

        locked_val.get() = 100;
        set_val = true;
        cv.notify_one();
      
    }
  }

  t1.join();

  auto val = synced_int.lock();

  CHECK(*val == 100);
}

TEST_CASE("Data Contention", "[race]") {
  seo::SyncVal<std::mutex, int> synced_int{};

  auto t1 = std::thread{[&](){
    for (unsigned int i = 0; i < 100000; i++) {
      auto locked_val = synced_int.lock();

        locked_val.get()++;
      
    }
  }};

  auto t2 = std::thread{[&](){
    for (unsigned int i = 0; i < 100000; i++) {
      auto locked_val = synced_int.lock();

        locked_val.get()--;
      
    }
  }};

  t1.join();
  t2.join();

  CHECK(*synced_int.lock() == 0);
}

void test_func(int& out_param)
{
  out_param++;
}




SCENARIO("Basic std::mutex Syncronization", "[std::mutex], [!mayfail]")
{
  GIVEN("A seo::SyncVal<std::mutex, int> constructed with value 0")
  {
    seo::SyncVal<std::mutex, int> synced_val{0};

    WHEN("You dereference the value constructed at 0.")
    {
      THEN("The value is 0.")
      {
        CHECK(*(synced_val.lock()) == 0);
      }
    }

    WHEN("You use the dereference operator to assign 1 to the value.")
    {
      *(synced_val.lock()) = 1;

      THEN("The value dereferences to 1.")
      {
        CHECK(*(synced_val.lock()) == 1);
      }
    }

    WHEN("You use the get() function to assign 100 to the value.")
    {
      synced_val.lock().get() = 100;

      THEN("The value dereferences to 100.")
      {
        CHECK(synced_val.lock().get() == 100);
      }
    }

    WHEN("The value is locked by another thread.")
    {
      THEN("try_lock returns seo::nullopt")
      {
        auto handle = std::thread([&]() {
          auto val_handle = synced_val.lock();
          std::this_thread::sleep_for(std::chrono::seconds(2));
        });
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        CHECK(synced_val.try_lock() == seo::nullopt);
        handle.join();
      }
    }

    WHEN("The value is unlocked.")
    {
      THEN("try_lock successfullly locks.")
      {
        CHECK(synced_val.try_lock().has_value() == true);
      }
    }
  }
}

SCENARIO("Basic std::timed_mutex Syncronization", "[std::timed_mutex], [!mayfail]")
{
  GIVEN("A seo::SyncVal<std::timed_mutex, int> constructed with value 0")
  {
    seo::SyncVal<std::timed_mutex, int> synced_val{0};

    WHEN("You dereference the value constructed at 0.")
    {
      THEN("The value is 0.")
      {
        CHECK(*(synced_val.lock()) == 0);
      }
    }

    WHEN("You use the dereference operator to assign 1 to the value.")
    {
      *(synced_val.lock()) = 1;

      THEN("The value dereferences to 1.")
      {
        CHECK(*(synced_val.lock()) == 1);
      }
    }

    WHEN("You use the get() function to assign 100 to the value.")
    {
      synced_val.lock().get() = 100;

      THEN("The value dereferences to 100.")
      {
        CHECK(synced_val.lock().get() == 100);
      }
    }

    WHEN("The value is locked by another thread.")
    {
      auto handle = std::thread([&]() {
        {
          auto val_handle = synced_val.lock();
          std::this_thread::sleep_for(std::chrono::seconds(5));
        }
      });

      THEN("try_lock returns seo::nullopt")
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        CHECK(synced_val.try_lock() == seo::nullopt);
      }

      THEN("try_lock_for returns seo::nullopt if timeout less than other thread holds lock.")
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        CHECK(synced_val.try_lock_for(std::chrono::seconds(2)) == seo::nullopt);
      }

      THEN("try_lock_until returns seo::nullopt if endpoint is less than other thread holds locks.")
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        CHECK(synced_val.try_lock_until(std::chrono::system_clock::now() +
                                        std::chrono::seconds(2)) == seo::nullopt);
      }

      THEN("try_lock_for returns value if timeout greater than other thread holds lock.")
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        CHECK(synced_val.try_lock_for(std::chrono::seconds(6)).has_value() == true);
      }

      THEN("try_lock_until returns value if endpoint is greater than other thread holds locks.")
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        CHECK(synced_val.try_lock_until(std::chrono::system_clock::now() + std::chrono::seconds(6))
                  .has_value() == true);
      }

      handle.join();
    }

    WHEN("The value is unlocked.")
    {
      THEN("try_lock successfullly locks.")
      {
        CHECK(synced_val.try_lock().has_value() == true);
      }
    }
  }
}

SCENARIO("Basic std::recursive_mutex Syncronization", "[std::recursive_mutex], [!mayfail]")
{
  GIVEN("A seo::SyncVal<std::recursive_mutex, int> constructed with value 0")
  {
    seo::SyncVal<std::recursive_mutex, int> synced_val{0};

    WHEN("You dereference the value constructed at 0.")
    {
      THEN("The value is 0.")
      {
        CHECK(*(synced_val.lock()) == 0);
      }
    }

    WHEN("You use the dereference operator to assign 1 to the value.")
    {
      *(synced_val.lock()) = 1;

      THEN("The value dereferences to 1.")
      {
        CHECK(*(synced_val.lock()) == 1);
      }
    }

    WHEN("You use the get() function to assign 100 to the value.")
    {
      synced_val.lock().get() = 100;

      THEN("The value dereferences to 100.")
      {
        CHECK(synced_val.lock().get() == 100);
      }
    }

    WHEN("The value is locked by another thread.")
    {
      THEN("try_lock returns seo::nullopt")
      {
        auto handle = std::thread([&]() {
          auto val_handle = synced_val.lock();
          std::this_thread::sleep_for(std::chrono::seconds(2));
        });
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        CHECK(synced_val.try_lock() == seo::nullopt);
        handle.join();
      }
    }

    WHEN("The value is unlocked.")
    {
      THEN("try_lock successfullly locks.")
      {
        CHECK(synced_val.try_lock().has_value() == true);
      }
    }

    WHEN("The value is locked in the current thread.")
    {
      THEN("lock will successfully lock again.")
      {
        auto locked_val  = synced_val.lock();
        auto locked_val2 = synced_val.lock();

        *locked_val = 100;

        CHECK(locked_val.get() == 100);
        CHECK(locked_val2.get() == 100);

        *locked_val = -100;

        CHECK(locked_val.get() == -100);
        CHECK(locked_val2.get() == -100);
      }
    }
  }
}

SCENARIO("Basic std::recursive_timed_mutex Syncronization",
         "[std::recursive_timed_mutex], [!mayfail]")
{
  GIVEN("A seo::SyncVal<std::recursive_timed_mutex, int> constructed with value 0")
  {
    seo::SyncVal<std::recursive_timed_mutex, int> synced_val{0};

    WHEN("You dereference the value constructed at 0.")
    {
      THEN("The value is 0.")
      {
        CHECK(*(synced_val.lock()) == 0);
      }
    }

    WHEN("You use the dereference operator to assign 1 to the value.")
    {
      *(synced_val.lock()) = 1;

      THEN("The value dereferences to 1.")
      {
        CHECK(*(synced_val.lock()) == 1);
      }
    }

    WHEN("You use the get() function to assign 100 to the value.")
    {
      synced_val.lock().get() = 100;

      THEN("The value dereferences to 100.")
      {
        CHECK(synced_val.lock().get() == 100);
      }
    }

    WHEN("The value is locked by another thread.")
    {
      auto handle = std::thread([&]() {
        auto val_handle = synced_val.lock();
        std::this_thread::sleep_for(std::chrono::seconds(5));
      });

      THEN("try_lock returns seo::nullopt")
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        CHECK(synced_val.try_lock() == seo::nullopt);
      }

      THEN("try_lock_for returns seo::nullopt if timeout less than other thread holds lock.")
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        CHECK(synced_val.try_lock_for(std::chrono::seconds(2)) == seo::nullopt);
      }

      THEN("try_lock_until returns seo::nullopt if endpoint is less than other thread holds locks.")
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        CHECK(synced_val.try_lock_until(std::chrono::system_clock::now() +
                                        std::chrono::seconds(2)) == seo::nullopt);
      }

      THEN("try_lock_for returns value if timeout greater than other thread holds lock.")
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        CHECK(synced_val.try_lock_for(std::chrono::seconds(6)).has_value() == true);
      }

      THEN("try_lock_until returns value if endpoint is greater than other thread holds locks.")
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        CHECK(synced_val.try_lock_until(std::chrono::system_clock::now() + std::chrono::seconds(6))
                  .has_value() == true);
      }

      handle.join();
    }

    WHEN("The value is unlocked.")
    {
      THEN("try_lock successfullly locks.")
      {
        CHECK(synced_val.try_lock().has_value() == true);
      }
    }

    WHEN("The value is locked in the current thread.")
    {
      THEN("lock will successfully lock again.")
      {
        auto locked_val  = synced_val.lock();
        auto locked_val2 = synced_val.lock();

        *locked_val = 100;

        CHECK(locked_val.get() == 100);
        CHECK(locked_val2.get() == 100);

        *locked_val = -100;

        CHECK(locked_val.get() == -100);
        CHECK(locked_val2.get() == -100);
      }
    }
  }
}
#if __cplusplus >= 201703L
SCENARIO("Basic std::shared_mutex Syncronization", "[std::shared_mutex], [!mayfail]")
{
  GIVEN("A seo::SyncVal<std::shared_mutex, int> constructed with value 0")
  {
    auto synced_val = seo::SyncVal<std::shared_mutex, int>{0};

    WHEN("You dereference the value constructed at 0.")
    {
      THEN("The value is 0.")
      {
        CHECK(*(synced_val.lock()) == 0);
      }
    }

    WHEN("You use the dereference operator to assign 1 to the value.")
    {
      *(synced_val.lock()) = 1;

      THEN("The value dereferences to 1.")
      {
        CHECK(*(synced_val.lock()) == 1);
      }
    }

    WHEN("You use the get() function to assign 100 to the value.")
    {
      synced_val.lock().get() = 100;

      THEN("The value dereferences to 100.")
      {
        CHECK(synced_val.lock().get() == 100);
      }
    }

    WHEN("The value is locked by another thread.")
    {
      auto handle = std::thread([&]() {
        auto val_handle = synced_val.lock();
        std::this_thread::sleep_for(std::chrono::seconds(2));
      });

      THEN("try_lock returns seo::nullopt")
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        CHECK(synced_val.try_lock() == seo::nullopt);
      }

      handle.join();
    }

    WHEN("The value is shared_locked by another thread.")
    {
      auto handle = std::thread([&]() {
        auto val_handle = synced_val.lock_shared();
        std::this_thread::sleep_for(std::chrono::seconds(2));
      });

      THEN("try_lock returns seo::nullopt")
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        CHECK(synced_val.try_lock() == seo::nullopt);
      }

      THEN("lock_shared returns value.")
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        CHECK(synced_val.lock_shared().has_value() == true);
      }

      THEN("try_lock_shared returns value.")
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        CHECK(synced_val.try_lock_shared().has_value() == true);
      }

      // This shouldn't compile
      // *(synced_val.lock_shared().value()) = 500;

      handle.join();
    }

    WHEN("The value is unlocked.")
    {
      THEN("try_lock successfullly locks.")
      {
        CHECK(synced_val.try_lock().has_value() == true);
      }
    }
  }
}
#endif
#if __cplusplus >= 201402L
SCENARIO("Basic std::shared_timed_mutex Syncronization", "[std::shared_timed_mutex], [!mayfail]")
{
  GIVEN("A seo::SyncVal<std::shared_timed_mutex, int> constructed with value 0")
  {
    seo::SyncVal<std::shared_timed_mutex, int> synced_val{0};

    WHEN("You dereference the value constructed at 0.")
    {
      THEN("The value is 0.")
      {
        CHECK(*(synced_val.lock()) == 0);
      }
    }

    WHEN("You use the dereference operator to assign 1 to the value.")
    {
      *(synced_val.lock()) = 1;

      THEN("The value dereferences to 1.")
      {
        CHECK(*(synced_val.lock()) == 1);
      }
    }

    WHEN("You use the get() function to assign 100 to the value.")
    {
      synced_val.lock().get() = 100;

      THEN("The value dereferences to 100.")
      {
        CHECK(synced_val.lock().get() == 100);
      }
    }

    WHEN("The value is locked by another thread.")
    {
      auto handle = std::thread([&]() {
        auto val_handle = synced_val.lock();
        std::this_thread::sleep_for(std::chrono::seconds(5));
      });

      THEN("try_lock returns seo::nullopt")
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        CHECK(synced_val.try_lock() == seo::nullopt);
      }

      THEN("try_lock_for returns seo::nullopt if timeout less than other thread holds lock.")
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        CHECK(synced_val.try_lock_for(std::chrono::seconds(2)) == seo::nullopt);
      }

      THEN("try_lock_until returns seo::nullopt if endpoint is less than other thread holds locks.")
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        CHECK(synced_val.try_lock_until(std::chrono::system_clock::now() +
                                        std::chrono::seconds(2)) == seo::nullopt);
      }

      THEN("try_lock_for returns value if timeout greater than other thread holds lock.")
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        CHECK(synced_val.try_lock_for(std::chrono::seconds(6)).has_value() == true);
      }

      THEN("try_lock_until returns value if endpoint is greater than other thread holds locks.")
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        CHECK(synced_val.try_lock_until(std::chrono::system_clock::now() + std::chrono::seconds(6))
                  .has_value() == true);
      }

      THEN("try_lock_shared returns nullopt.")
      {
        CHECK(synced_val.try_lock_shared().has_value() == false);
      }

      THEN("try_lock_shared_for returns nullopt.")
      {
        CHECK(synced_val.try_lock_shared_for(std::chrono::milliseconds(5)).has_value() == false);
      }

      THEN("try_lock_shared_until returns nullopt.")
      {
        CHECK(synced_val
                  .try_lock_shared_until(std::chrono::system_clock::now() +
                                         std::chrono::milliseconds(5))
                  .has_value() == false);
      }

      THEN("try_lock_shared_for returns value if timeout is greater than lock.")
      {
        CHECK(synced_val.try_lock_shared_for(std::chrono::seconds(6)).has_value() == true);
      }

      THEN("try_lock_shared_until returns value if endpoint is greater then lock.")
      {
        CHECK(synced_val
                  .try_lock_shared_until(std::chrono::system_clock::now() + std::chrono::seconds(6))
                  .has_value() == true);
      }

      handle.join();
    }

    WHEN("The value is shared_locked by another thread.")
    {
      auto handle = std::thread([&]() {
        auto val_handle = synced_val.lock_shared();
        std::this_thread::sleep_for(std::chrono::seconds(2));
      });
      std::this_thread::sleep_for(std::chrono::milliseconds(500));

      THEN("try_lock returns seo::nullopt")
      {
        CHECK(synced_val.try_lock() == seo::nullopt);
      }

      THEN("lock_shared returns value.")
      {
        CHECK(synced_val.lock_shared().has_value() == true);
      }

      THEN("try_lock_shared returns value.")
      {
        CHECK(synced_val.try_lock_shared().has_value() == true);
      }

      THEN("try_lock_shared_for returns value.")
      {
        CHECK(synced_val.try_lock_shared_for(std::chrono::milliseconds(5)).has_value() == true);
      }

      THEN("try_lock_shared_until returns value.")
      {
        CHECK(synced_val
                  .try_lock_shared_until(std::chrono::system_clock::now() +
                                         std::chrono::milliseconds(5))
                  .has_value() == true);
      }

      // This shouldn't compile
      // *(synced_val.lock_shared().value()) = 500;
      handle.join();
    }

    WHEN("The value is unlocked.")
    {
      THEN("try_lock successfullly locks.")
      {
        CHECK(synced_val.try_lock().has_value() == true);
      }
    }
  }
}
#endif
SCENARIO("Non-Copy non-Move Construction", "[construction]")
{
  GIVEN("A non-copyable object.")
  {
    class FOO {
    public:
      FOO()                 = delete;
      FOO(const FOO& other) = delete;
      FOO& operator=(const FOO& other) = delete;

      FOO(int x) : val{x}
      {
      }
      FOO(FOO& other) = delete;
      FOO& operator=(FOO& other) = delete;
      auto get() -> int
      {
        return val;
      }

    private:
      int val = 0;
    };
    WHEN("You call the seo::SyncVal Constructor:")
    {
      THEN("The constructor properly constructs the object in-place.")
      {
        seo::SyncVal<std::mutex, FOO> synced_val{1};

        CHECK(synced_val.lock()->get() == 1);
      }
    }
  };
}