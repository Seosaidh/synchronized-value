# Synchronized Value

## Overview

Provides a template for a value-type that can only be accessed through a mutex.

C++11 added threading to the standard library, and it included mutex primatives to allow
protected data shared between threads. However, the mutexes are separate variables from
the data that they protect. This means that it is possible to lock the wrong mutex when
accessing the data. Additionally, even if the mutex is bundled together into a struct
with the data that it protects, it is still possible for the programmer to forget to lock
the mutex before handling the data. Of course, if a mistake *can* happen, it *will* eventually
happen, given enough code and enough programmers.

This class template fixes the problem by bundling the value as a private member of a class.
The value is only accessible through calling a `lock` function, which ensures that the mutex
is locked before the data is accessed. Additionally, the `lock` function returns a helper
class that acts like a pointer into the value, while holding the mutex lock. When the
destructor of this class is called (signalling that the data is no longer available), the
mutex is automatically unlocked. This ensures that the mutex remains locked for the duration
of the data access.

This methodology is inspired by Rusts [std::sync::Mutex](https://doc.rust-lang.org/std/sync/struct.Mutex.html).

This library will be contained within a single header file: seo_sync_val.hpp.
All other files are for tests, license, etc.

## Dependencies

This class depends upon and uses [Simon Brand's Optional](https://github.com/TartanLlama/optional)
by default. This can be easily switched to `std::optional` if it is available by simply
defining the STD_OPTIONAL pre-processor directive when compiling:
```bash
g++ -Isome_path_to_seo_sync_val_dir -DSTD_OPTIONAL -o app_name app_source.cpp
```

`tl::optional` is used for the return value of the various lock*() functions.
The reason for this is that the lock*() functions actually return a special
class that is used to manage the mutex lock and automatically unlock it when the
access to the value goes out of scope. As such, they cannot return true/false
like the std::*mutex functions do. And since `std::optional` is only available on C++17-compliant
compilers, and better backwards compatability was desired, `tl::optional` was chosen.

## Tested Compilers

The library is tested against the following compilers on Ubuntu 18.04 to ensure valid compilation:

* GCC 4.8
* GCC 5
* GCC 6
* GCC 7
* GCC 8
* GCC 9
* Clang 9

In general, if your compiler fully supports C++11, then this library should work with `tl::optional`.
If the compiler supports `std::optional`, then it should work with `STD_OPTIONAL` defined.

## Instructions for use

To use, simply put the seo_sync_val.hpp file into the include path (as well as Simon Brand's
optional header if using that), then
```c++
#include <seo_sync_val.hpp>
```

The class is `seo::SyncVal`. The class is templated on two types: The mutex type
that guards the value and the value guarded. The latter is obvious, the
former is to allow adding/removing certain overloads based on the type of mutex.
I.E. `seo::SyncVal` will allow calling a `try_lock_for()` call if the mutex is a 
`std::timed_mutex`, but not if a `std::mutex` is used.

The usage is fairly simple. You create the `seo::SyncVal` by passing an r-value
of some value type or passing the arguments for a value's constructor directly to the 
constructor for `seo::SyncVal`.

Unfortunately, at this time, template argument deduction doesn't work, as none of the mutex
types are movable or copyable, so they cannot be passed into the constructor.

For example: 
```c++
auto synced_string  = seo::SyncVal<std::mutex, std::string>{{}};
auto synced_int     = seo::SyncVal<std::timed_mutex, int>{1};
auto synced_vec     = seo::SyncVal<std::mutex, std::vector<int>>{100, 5};
auto synved_vec2    = seo::SyncVal<std::mutex, std::vector<int>>{std::vector{100, 5}};
```

Once created, you can call any of the lock*() functions supported by the mutex
used. So if the `seo::SyncVal` was created with a `std::mutex`, then you have
access to:
```c++
seo::SyncVal::lock();
seo::SyncVal::try_lock();
```
If you used a `std::shared_mutex`, you can call:
```c++
seo::SyncVal::lock();
seo::SyncVal::try_lock();
seo::SyncVal::lock_shared();
seo::SyncVal::try_lock_shared();
```

All lock function return an `seo::optional<seo::SyncValHandle<T>>`. Please see Simon's
[documentation](https://tl.tartanllama.xyz/en/latest/api/optional.html) for details
on how to handle `tl::optional<T>`. The `seo::SyncValHandle<T>` behaves like a pointer-type
to the protected value.

There is also a const version of `seo::SyncValHandle<T>` that is returned when
`lock_shared()` or `try_lock_shared()` returns. This version only gives `const` pointers
or references instead of mutable ones.

## Simple Example

```c++
#include <seo_sync_val.hpp>
#include <memory>
#include <shared_mutex>
struct SharedStruct {
    int some_data;
    double more_data;
};

int main() {
    auto shared_data = std::make_shared<seo::SyncVal<std::shared_mutex, SharedStruct>>
        (SharedStruct{1, 1.0});

    {
        auto data_handle = shared_data->lock();

        data_handle->more_data = 2.0;
        data_handle->some_data = -500;
    } // data_handle goes out of scope here, and the mutex is unlocked here

    return 0;
}
```

Further details can be found at the [documentation](https://seosaidh.gitlab.io/synchronized-value).