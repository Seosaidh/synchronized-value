.. Synchronized Value documentation master file, created by
   sphinx-quickstart on Wed Apr  8 23:06:53 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

##################
Synchronized Value
##################

.. _intro_label:

************
Introduction
************

This project aims at simplifying the synchronization of values across threads by bundling
the value to be synchronized with the mutex used to protect it. Additionally, the bundled
value can only be accessed through locking the mutex. Finally, the various lock functions
provide a handle that acts like a reference to the value, but when the destructor is called,
the mutex is automatically unlocked.

In other words, this is a combination of a ``mutex``, ``std::lock_guard``, and a value all
rolled into a single interface.

.. _motivation_label:

**********
Motivation
**********

C++11 added `threading <https://en.cppreference.com/w/cpp/thread>`_ to the standard library, and it included mutex primatives to allow
protected data sharing between threads. However, the mutexes are separate variables from
the data that they protect. This means that it is possible to lock the wrong mutex when
accessing the data. Additionally, even if the mutex is bundled together into a struct
with the data that it protects, it is still possible for the programmer to forget to lock
the mutex before handling the data. Of course, if a mistake *can* happen, it *will* eventually
happen, given enough code and enough programmers.

.. _inspiration_label:

***********
Inspiration
***********

In Rust, the `Mutex <https://doc.rust-lang.org/std/sync/struct.Mutex.html>`_ is a template class that wraps the data that it protects.
Once constructed, the data can only be accessed by calling the Mutex::lock()
method. This locks the mutex, then returns a structure that automatically
dereferences into the value type wrapped by the Mutex. When the structure goes
out of scope, its destructor will automatically unlock the mutex. In this way,
there is no possibility of forgetting to lock the mutex before you access the
data, and there is no possibility of forgetting to unlock the mutex when you are
done. And even better, there is no possibility of locking the *wrong* mutex for
the data that you are using.

One success story that Rust can tout is that Mozilla tried twice over several years to
make the CSS engine in Firefox multi-threaded. Both times, the attempts (in C++) failed.
Once Rust was deemed mature enough, they made an attempt to perform this task using Rust.
They were successful within a year.

This project attempts to make synchronizing values across threads as easy in C++ as it is
in Rust by copying the method Rust uses to synchronize values.

One drawback of the Mutex class in Rust is that it doesn't implement multiple
readers. This is done by a different class: `RwLock <https://doc.rust-lang.org/std/sync/struct.RwLock.html>`_.
This was likely done for performance reasons. In this implementation, the :ref:`syncval_label`
class templates on the mutex type so that it can be used with all the various C++
mutex types to provide versatile functionality as needed by the developer.

In addition to Rust's Mutex type. This project takes some inspiration from Facebook's
`folly::Synchronized <https://github.com/facebook/folly/blob/master/folly/docs/Synchronized.md>`_.
The inspiration mainly came in how to handle the :ref:`syncguard_label`'s control of the mutex, and a reminder
that an API to allow using ``std::condition_variable`` would be desirable.

.. _download_label:

********
Download
********

:download:`Latest Header Source <../seo_sync_val.hpp>` 

`Release Page <https://gitlab.com/Seosaidh/synchronized-value/-/releases>`_

.. _issue_label:

******************************
Feature Request and Bug Report
******************************

In order to report a bug or request a feature, either
`send an email <mailto:incoming+seosaidh-synchronized-value-17658366-issue-@incoming.gitlab.com>`_
with the issue title as the email subject and a detailed description in the email body, or
visit our Gitlab `issues page <https://gitlab.com/Seosaidh/synchronized-value/-/issues>`_.

.. _compatibility_label:

**********************
Compiler Compatibility
**********************

This library should be compatible with any C++11 or greater compliant compiler. The following compilers are
specifically tested (on Ubuntu 18.04) and guaranteed to work:

* GCC 4.8
* GCC 5
* GCC 6
* GCC 7
* GCC 8
* GCC 9
* Clang 9

.. _documentation_label:

*************
Documentation
*************

=================
Project Structure
=================

There are two classes that make up the Synchronized Value project: :ref:`syncval_label` and
:ref:`syncguard_label`. The :ref:`syncval_label` is used to actually wrap the value to be
synchronized. All copy and move constructors/assignment have been disabled, similar to the 
``mutex`` classes in C++, since it owns a mutex.

:ref:`syncguard_label` is used to provide access to the data wrapped by :ref:`syncval_label`
as well as control how long the mutex remains locked. While the constructor for :ref:`syncguard_label`
is public to allow it to be wrapped by a ``tl::optional`` or ``std::optional``, it should
never be directly constructed; it is instead provided by the various ``lock`` functions of
:ref:`syncval_label`. :ref:`syncguard_label` exhibits pointer semantics when moved. I.E. when moved
from or to, the reference and mutex guard are moved. This prevents moving between :ref:`syncguard_label`
that contain different types or mutexes.

Further details regarding these classes can be found in their documentation below.

====================
General Instructions
====================

To use, simply put the seo_sync_val.hpp file into the include path (as well as Simon Brand's
optional header if using that), then::

   #include <seo_sync_val.hpp>

The main class is :ref:`syncval_label`. The class is templated on two types: The mutex type
that guards the value and the value type guarded. The latter is obvious, the
former is to allow adding/removing certain overloads based on the type of mutex.
I.E. :ref:`syncval_label` will allow calling ``try_lock_for()`` if the mutex is a 
``std::timed_mutex``, but not if a ``std::mutex`` is used.

The usage is fairly simple. You create the :ref:`syncval_label` by passing an r-value
of some value type or passing the arguments for a value's constructor directly to the 
constructor for :ref:`syncval_label` (including no arguments if the value is
default-constructable).

Unfortunately, at this time, template argument deduction doesn't work, as none of the mutex
types are movable or copyable, so they cannot be passed into the constructor. This could
be fixed by allowing the :ref:`syncval_label` to take a reference to a mutex instead of
owning the mutex, but in order to prevent any additional lifetime problems, the decision
was made to allow :ref:`syncval_label` to own the mutex.

Construction example::

   auto synced_string  = seo::SyncVal<std::mutex, std::string>{{}};
   auto synced_int     = seo::SyncVal<std::timed_mutex, int>{1};
   auto synced_vec     = seo::SyncVal<std::mutex, std::vector<int>>{100, 5};
   auto synved_vec2    = seo::SyncVal<std::mutex, std::vector<int>>{std::vector{100, 5}};


Once created, you can call any of the ``lock*()`` functions supported by the mutex
used. So if the :ref:`syncval_label` was created with a ``std::mutex``, then you have
access to:

* :cpp:any:`seo::SyncVal::lock()`
* :cpp:any:`seo::SyncVal::try_lock()`

If you used a ``std::shared_mutex``, you can call:

* :cpp:any:`seo::SyncVal::lock()`
* :cpp:any:`seo::SyncVal::try_lock()`
* :cpp:any:`seo::SyncVal::lock_shared()`
* :cpp:any:`seo::SyncVal::try_lock_shared()`


All lock function return an ``optional<seo::SyncValHandle<G,T,C>>``. Please see Simon's
`documentation <https://tl.tartanllama.xyz/en/latest/api/optional.html>`_ for details
on how to handle ``tl::optional<T>``. The :ref:`syncguard_label` implements
``operator()``, ``operator->``, ``operator*``, and ``&T get()``; as well as the various
``std::condition_variable`` calls (with a provided condition variable), and several convenience
operators.

There is also a const version of :ref:`syncguard_label` that is returned when
:cpp:any:`seo::SyncVal::lock_shared()`, :cpp:any:`seo::SyncVal::try_lock_shared()`,
:cpp:any:`seo::SyncVal::try_lock_shared_for()`, or 
:cpp:any:`seo::SyncVal::try_lock_shared_until()` returns.
This version only gives ``const`` pointers or references instead of mutable ones.

-------------
Customization
-------------

If available and desired, the :ref:`syncval_label` can be easily changed to use ``std::optional``
instead of ``tl::optional``. All that must be done is define ``STD_OPTIONAL`` during your
build, like so:

.. code-block:: bash

   g++ -Isome_path_to_seo_sync_val_dir -DSTD_OPTIONAL -o app_name app_source.cpp

Throughout the documentation, there will be references to ``seo::nullopt`` and "optional type".
The "optional type" refers to whichever optional type is being used. ``seo::nullopt`` is a
valid descriptor of whichever nullopt value is in the using statement within the ``seo``
namespace (default ``tl::nullopt``).

=======
Example
=======

::

   #include <seo_sync_val.hpp>
   #include <memory>
   #include <shared_mutex>
   #include <thread>

   struct SharedStruct {
       int some_data;
       double more_data;
   };

   int main() {
       // Construct the shared data pointer.
       auto shared_data = std::make_shared<seo::SyncVal<std::shared_mutex, SharedStruct>>
           (SharedStruct{1, 1.0});

       // Spawn a thread that takes a copy of the pointer and then will manipulate the
       // data over several iterations of a loop.
       auto thread1 = std::thread{[=](){
            for (unsigned int counter = 0; counter < 100000; counter++) {
               // The lock is aquired on the next line and held until the value variable
               // goes out of scope.
               auto data = shared_data->lock();
               
               if (data->some_data > -500) {
                  data->some_data--;
               }

               if (data->more_data >= 2.0) {
                  data->more_data = data->more_data + 1.0;
               }
            } // data goes out of scope here, and so the lock is released.
       }};

       // Concurrently with the above thread, access the data and just set it to certain
       // values.
       {
         auto data_handle = shared_data->lock();
         data_handle->more_data = 2.0;
         data_handle->some_data = -500;
       } // data_handle goes out of scope here, and the mutex is unlocked here.

       thread1.join();

       return 0;
   }

.. _syncval_label:

============
seo::SyncVal
============

.. doxygenclass:: seo::SyncVal
   :members:

.. _syncguard_label:

=================
seo::SyncValGuard
=================

.. doxygenclass:: seo::SyncValGuard
   :members:


==============
Free Functions
==============

.. doxygenfunction:: seo::lock(SyncVal<M1, T1>&, SyncVal<M2, T2>&)

.. doxygenfunction:: seo::try_lock(SyncVal<M1, T1>&, SyncVal<M2, T2>&)


.. rubric:: Footnotes

.. [#f1] Logo made by `DesignEvo free logo creator <https://www.designevo.com/logo-maker/>`_