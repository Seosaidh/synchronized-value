#include <type_traits>  // Needed for SFINAE stuff

// Allow switching to std::optional if STD_OPTIONAL is defined.
#ifdef STD_OPTIONAL
  #include <optional>
#else
  #include <tl/optional.hpp>
#endif

#include <utility>  // Needed for std::pair
#include <cassert>  // Needed for assert()
#include <mutex>    // Needed for mutex type, used in SFINAE checks
#if __cplusplus >= 201402L
  #include <shared_mutex>  // Same as <mutex>
#endif

#include <tuple>  // Needed for std::tuple, which is returned by the lock and try_lock functions.

namespace seo {

// Switch to std::optional if STD_OPTIONAL is defined. All refrences to these things
// should not be fully-qualified, so that these using statements can properly switch which we are
// using.
#ifdef STD_OPTIONAL
using std::in_place;
using std::nullopt;
using std::optional;
#else
using ::tl::in_place;
using ::tl::nullopt;
using ::tl::optional;
#endif

// std::shared_timed_mutex was added in C++ 14. So when we are in C++14 or better mode,
// then set IsSharedTimedMutex to the regular is_same. Otherwise, have is_same always
// return false.
#if __cplusplus >= 201402L
template<typename Mutex>
using IsSharedTimedMutex = std::is_same<Mutex, std::shared_timed_mutex>;
#else
template<typename Mutex>
using IsSharedTimedMutex = std::false_type;
#endif

// std:shared_mutex was added in C++17. So when we are in C++17 or better mode, check
// mutex as normal. Otherwise, always set std::is_same to false.
#if __cplusplus >= 201703L
template<typename Mutex>
using IsRawSharedMutex = std::is_same<Mutex, std::shared_mutex>;
#else
template<typename Mutex>
using IsRawSharedMutex = std::false_type;
#endif

template<typename Mutex, typename Dummy>
using IsTimedMutex =
    typename std::enable_if<std::is_same<Mutex, std::timed_mutex>::value ||
                                std::is_same<Mutex, std::recursive_timed_mutex>::value ||
                                IsSharedTimedMutex<Mutex>::value,
                            Dummy>::type;

template<typename Mutex, typename Dummy>
using IsSharedMutex =
    typename std::enable_if<IsRawSharedMutex<Mutex>::value || IsSharedTimedMutex<Mutex>::value,
                            Dummy>::type;

template<typename Type, typename Dummy>
using IsTriviallyConstructable =
    typename std::enable_if<std::is_default_constructible<Type>::value, Dummy>::type;

template<typename Guard, typename Dummy>
using IsCvCompatible =
    typename std::enable_if<std::is_same<Guard, std::unique_lock<std::mutex>>::value, Dummy>::type;

// If we don't have C++17 or better, we need to define std::void ourselves. If we do have it,
// just use it.
#if __cplusplus >= 201703L
using std::void_t;
#else
template<typename...>
using void_t = void;
#endif

#if __cplusplus >= 201402L
template<typename Mutex>
using SharedLock = std::shared_lock<Mutex>;
#else
// When GCC 4.8 tries to compile, it still seems to see the SFINAE lock_shared*() function internals
// and sees that the type doesn't have members try_lock, try_lock_for, etc.
// As such, provide unique_lock instead. It won't ever be used, because those functions won't be
// part of the overload set in C++11, because the IsSharedMutex will be std::false_type
template<typename Mutex>
using SharedLock = std::unique_lock<Mutex>;
#endif

// Forward-declare so that SyncVal can refer to it in return statements.
template<typename Guard, typename Type, bool CONST = true>
class SyncValGuard;

/**
 * @brief Main Syncronized Value Class
 *
 * @rst
 * This is the main class of the Synchronized Value library. It wraps any value type
 * and pairs it with any of the C++ standard mutex types. It then provides access to
 * the wrapped data only through locking the associated mutex.
 *
 * This class cannot be copied or moved, as it owns a mutex, which disables copy/move
 * semantics in C++.
 *
 * Once constructed, the class hides the data and provides no access to it. However,
 * when a lock() function is called, it will return an instance of :ref:`syncguard_label`,
 * which will provide access to the data and then automatically unlock the mutex when
 * it goes out of scope. (See :ref:`syncguard_label` for details).
 *
 * As such, the general workflow is to create a :ref:`syncval_label` and then share it as needed.
 * When the data is needed, an appropriate lock() function is called and then the
 * newly-constructed :ref:`syncguard_label` is used to actually read/write the data.
 * @endrst
 *
 * @tparam Mutex Some mutex type
 * @tparam Type Any value type
 */
template<typename Mutex, typename Type>
class SyncVal final {
private:
  Mutex mutex_val;
  Type  value;

public:
  // Not Copyable, or Movable
  // Only allow explicit construction directly.
  SyncVal(SyncVal&& sv)      = delete;
  SyncVal(const SyncVal& sv) = delete;
  auto operator=(const SyncVal&) -> SyncVal& = delete;
  auto operator=(SyncVal &&) -> SyncVal& = delete;
  ~SyncVal()                             = default;

  /**
   * @brief Construct a new SyncVal object
   *
   * @rst
   * Many of the template parameters, on the member functions of this class as well
   * as :ref:`syncguard_label` follow this format of a dummy parameter defaulted to void
   * and then a Concept-like typedef that wraps std::enable_if to allow SFINAE.
   * As such, no other functions will have their template parameters documented, as
   * it clutters up the documentation and is not interesting to the end user.
   *
   * This is the default constructor for :ref:`syncval_label`. It is only enabled if the value
   * being wrapped has a default constructor (trivially constructable).
   * @endrst
   *
   * @tparam Dummy Dummy parameter used to force IsTriviallyConstructable to work with SFINAE
   *
   * @rst
   * **Example**::
   *
   *  auto synced_val = std::make_shared<seo::SyncVal<std::mutex, std::string>>();
   *
   * @endrst
   */
  template<typename Dummy = void, typename = IsTriviallyConstructable<Type, Dummy>>
  explicit constexpr SyncVal() noexcept(std::is_nothrow_constructible<Type>::value) :
      mutex_val{}, value{}
  {
  }

  /**
   * @brief Construct a new SyncVal object from an r-value
   *
   * @param val_param The value to wrap (should be r-value, but std::forward used just in case)
   *
   * @rst
   * This constructor takes an r-value of the Type being wrapped and wraps that value
   * in the :ref:`syncval_label` class.
   *
   * **Example**::
   *
   *  auto synced_val = std::make_shared<seo::SyncVal<std::mutex, std::string>>(std::string("Hello
   *  World!"));
   *
   * @endrst
   */
  explicit constexpr SyncVal(Type&& val_param) noexcept(
      std::is_nothrow_constructible<Type>::value) :
      mutex_val{}, value{std::forward<Type>(val_param)}
  {
  }

  /**
   * @brief Construct a new Sync Val object by calling the value's constructor
   *
   * @tparam Types The various types of the constructor being called
   * @param Args The arguments to one of the values constructor. These will be forwarded
   * to the value's constructor.
   *
   * @rst
   * This constructor will call the value's construtor in-place. This allows SyncVal
   * to wrap a non-movable/non-copyable value. This constructor takes the arguments
   * for one of the value's constructors and simply forwards them to the value's
   * constructor.
   *
   * **Example**::
   *
   *  auto synced_val = std::make_shared<seo::SyncVal<std::mutex, std::string>>("Hello World!");
   *
   * @endrst
   */
  template<typename... Types>
  constexpr SyncVal(Types&&... Args) noexcept(std::is_nothrow_constructible<Type>::value) :
      mutex_val{}, value{std::forward<Types>(Args)...}
  {
  }

  /**
   * @brief Lock the mutex and return a handle to make the data accessible.
   *
   * @return optional<SyncValGuard<unique_lock, Type, false>>
   *
   * @rst
   * Locks the mutex and returns a handle to the data. If another thread has already
   * locked the mutex, this call will block execution until the lock is aquired.
   *
   * If this function is called by a thread that has already called lock, the behavior
   * is undefined. The program may deadlock. It may also simply return ``seo::nullopt``.
   *
   * Please see the documentation for :ref:`syncguard_label` for details on how to use
   * the return value of this function.
   *
   * **Example**::
   *
   *  #include <seo_sync_val.hpp>
   *  #include <mutex>
   *  #include <thread>
   *  #include <iostream>
   *  #include <memory>
   *
   *  int main() {
   *    auto synced_val = std::make_shared<seo::SyncVal<std::mutex, std::string>>("Hello World!");
   *    auto thread1 = std::thread{[=](){
   *        for (unsigned int i = 0; i < 3; i++){
   *            auto locked_val = synced_val->lock();
   *
   *            locked_val.get() = "thead 1 iteration " + std::to_string(i);
   *        }
   *    }};
   *
   *    auto thread2 = std::thread{[=](){
   *        for (unsigned int i = 0; i < 3; i++){
   *            auto locked_val = synced_val->lock();
   *
   *            locked_val.get() = "thead 2 iteration " + std::to_string(i);
   *        }
   *    }};
   *
   *    thread1.join();
   *    thread2.join();
   *
   *    auto locked_val = synced_val->lock();
   *
   *    // Possible Output: "thread 2 iteration 3" or "thread 1 iteration 3"
   *    std::cout << locked_val.get();
   *
   *    return 0;
   *  }
   *
   * @endrst
   */
  auto lock() -> SyncValGuard<std::unique_lock<Mutex>, Type, false>
  {
    return SyncValGuard<std::unique_lock<Mutex>, Type, false>{std::unique_lock<Mutex>(mutex_val),
                                                              &value};
  }

  /**
   * @brief Attempt to lock the mutex and return a handle to make the data accessible.
   *
   * @return optional<SyncValGuard<unique_lock, Type, false>>
   *
   * @rst
   * Tries to lock the mutex. This function will return immediately. On successful lock,
   * the function returns the :ref:`syncguard_label` wrapped in the optional type.
   * Otherwise, it returns ``seo::nullopt``. This function is allowed to spuriously
   * return ``seo::nullopt``, even if the mutex is not locked by any other thread.
   *
   * If this function is called by a thread that has already locked the mutex, the
   * behavior is undefined.
   *
   * Please see the documentation for :ref:`syncguard_label` for details on how to use
   * the return value of this function.
   *
   * **Example**::
   *
   *  #include <seo_sync_val.hpp>
   *  #include <mutex>
   *  #include <thread>
   *  #include <iostream>
   *  #include <chrono>
   *  #include <memory>
   *
   *  std::chrono::milliseconds interval {100};
   *
   *  int main() {
   *      auto shared_value = std::make_shared<seo::SyncVal<std::mutex, int>>(1);
   *
   *      auto t1 = std::thread{[=](){
   *          std::this_thread::sleep_for(interval);
   *
   *          for (unsigned int i = 0; i < 10000; i++) {
   *              auto locked_val = shared_value->try_lock();
   *             
   *              locked_val.get()++;
   *              std::cout << locked_val.get() << "\n";
   *          }
   *      }};
   *      {
   *          auto locked_value = shared_value->lock();
   *          
   *          *(locked_value) = 100;
   *          std::cout << *(locked_value) << "\n";
   *          
   *      }
   *      t1.join();
   *      return 0;
   *  }
   * @endrst
   */
  auto try_lock() noexcept -> optional<SyncValGuard<std::unique_lock<Mutex>, Type, false>>
  {
    // Don't need try/catch here, as try_lock() won't throw exceptions
    auto guard = std::unique_lock<Mutex>(mutex_val, std::defer_lock);
    if (guard.try_lock()) {
      return optional<SyncValGuard<std::unique_lock<Mutex>, Type, false>>(
          in_place, std::move(guard), &value);
    }
    return nullopt;
  }

  // clang-format off
  /**
   * @brief Try to lock the mutex for a certain amount of time before giving up
   *
   * @param timeout_duration The amount of time to keep trying to lock the mutex.
   * @return optional<SyncValGuard<std::unique_lock, Type, false>>
   *
   * @rst
   * Tries to lock the mutex. It blocks until the specified ``timeout_duration`` has passed,
   * or the lock is aquired, whichever comes first. On successful lock, the function
   * returns :ref:`syncguard_label` wrapped in an optional type, otherwise it returns
   * ``seo::nullopt``.
   *
   * If ``timeout_duration`` is less than or equal to ``timeout_duration.zero()``, the
   * the function behaves like :cpp:any:`seo::SyncVal::try_lock()`.
   *
   * The function may block longer than ``timeout_duration`` due to scheduling or
   * resource contention delays.
   *
   * The standard recommends using a ``steady_clock`` for the duration, otherwise the
   * wait time may be sensitive to clock adjustments.
   *
   * As with :cpp:any:`seo::SyncVal::try_lock()`, this function is allowed to fail
   * spuriously and return ``seo::nullopt`` even if the mutex was not locked by any
   * other thread at some point during ``timeout_duration``.
   *
   * If this function is called by a thread that has already aquired the mutex, the
   * behavior is undefined.
   *
   * This function is only available if the Mutex type used in the class template is
   * one of:
   *
   * * `std::timed_mutex <https://en.cppreference.com/w/cpp/thread/timed_mutex>`_
   * * `std::recursive_timed_mutex <https://en.cppreference.com/w/cpp/thread/recursive_timed_mutex>`_
   * * `std::shared_timed_mutex <https://en.cppreference.com/w/cpp/thread/shared_timed_mutex>`_
   *
   * Please see the documentation for :ref:`syncguard_label` for details on how to use
   * the return value of this function.
   * @endrst
   */
  // clang-format on
  template<typename Rep,
           typename Period,
           typename Dummy = void,
           typename       = IsTimedMutex<Mutex, Dummy>>
  auto try_lock_for(const std::chrono::duration<Rep, Period>& timeout_duration) noexcept
      -> optional<SyncValGuard<std::unique_lock<Mutex>, Type, false>>
  {
    auto guard = std::unique_lock<Mutex>(mutex_val, std::defer_lock);
    // Need the try/catch, as if we are given a non-std clock, that clock may throw. The
    // std clock will never throw.
    try {
      if (guard.try_lock_for(timeout_duration)) {
        return optional<SyncValGuard<std::unique_lock<Mutex>, Type, false>>(
            in_place, std::move(guard), &value);
      }
    }
    catch (...) {
      // Don't need to unlock the mutex, as we can only end up here from the try_lock_for()
      // call. If that fails, then the mutex is guaranteed to not be locked (by us).
      // We know that the construction of the optional is noexcept, because optional's constructor
      // is noexcept if the type it wraps has a noexcept constructor, which SyncValGuard does.
    }
    return nullopt;
  }

  // clang-format off
  /**
   * @brief Lock the mutex and return a handle to make the data accessible.
   *
   * @param timeout_time The point in time at which to stop trying to lock the mutex
   * @return optional<SyncValGuard<std::unique_lock<Mutex>, Type, false>>
   *
   * @rst
   * Tries to lock the mutex. Blocks until the specified ``timeout_time`` has been reached
   * or the lock is aquired, whichever comes first. On successful lock aquisition,
   * it returns :ref:`syncguard_label` wrapped in an optional type. Otherwise it
   * returns ``seo::nullopt``.
   *
   * If ``timeout_time`` has already passed, this function behaves like
   * :cpp:any:`seo::SyncVal::try_lock()`.
   *
   * The clock tied to ``timeout_time`` is used, which means that adjustements of
   * the clock are taken into account. Thus the maximum duration of the block might,
   * but might not, be more or less than ``timeout_time - Clock::now()`` at the time
   * of the call, depending on the duration of the adjustment. Additionally, the
   * function may block until after ``timeout_time`` due to scheduling or resource
   * contention delays.
   *
   * As with :cpp:any:`seo::SyncVal::try_lock()`, this function is allowed to fail spuriously and
   * return ``seo::nullopt`` even if the mutex was unlocked at some point before
   * ``timeout_time``.
   *
   * If the calling thread has already locked the mutex, the behavior is undefined.
   *
   * This function is only available if the Mutex type used in the class template is
   * one of:
   *
   * * `std::timed_mutex <https://en.cppreference.com/w/cpp/thread/timed_mutex>`_
   * * `std::recursive_timed_mutex <https://en.cppreference.com/w/cpp/thread/recursive_timed_mutex>`_
   * * `std::shared_timed_mutex <https://en.cppreference.com/w/cpp/thread/shared_timed_mutex>`_
   *
   * @endrst
   */
  // clang-format on
  template<typename Clock,
           typename Duration,
           typename Dummy = void,
           typename       = IsTimedMutex<Mutex, Dummy>>
  auto try_lock_until(const std::chrono::time_point<Clock, Duration>& timeout_time) noexcept
      -> optional<SyncValGuard<std::unique_lock<Mutex>, Type, false>>
  {
    auto guard = std::unique_lock<Mutex>(mutex_val, std::defer_lock);
    // Need the try/catch, as if we are given a non-std clock, that clock may throw. The
    // std clock will never throw.
    try {
      if (guard.try_lock_until(timeout_time)) {
        return optional<SyncValGuard<std::unique_lock<Mutex>, Type, false>>(
            in_place, std::move(guard), &value);
      }
    }
    catch (...) {
      // Don't need to unlock the mutex, as we can only end up here from the guard.try_lock_for()
      // call. If that fails, then the mutex is guaranteed to not be locked (by us).
      // We know that the construction of the optional is noexcept, because optional's constructor
      // is noexcept if the type it wraps has a noexcept constructor, which SyncValGuard does.
    }
    return nullopt;
  }

  /**
   * @brief Lock the mutex in read-only mode and return a handle to make the data accessible.
   *
   * @return optional<SyncValGuard<std::shared_lock<Mutex>, Type, true>>
   *
   * @rst
   * Aquires shared ownership of the mutex. If another thread is holding the mutex
   * with exclusive ownership (any lock function without "shared" in the function
   * name), a call to this function will block until shared ownership can be aquired.
   *
   * If this function is called by a thread that has already aquired the mutex (in
   * either shared or exclusive mode), the behavior is undefined.
   *
   * If more than the implementation-defined maximum number of shared owners already
   * locked the mutex in shared mode, then this function blocks until the number
   * of owners is reduced. This maximum is guaranteed to be at least 10,000.
   *
   * The :ref:`syncguard_label` that is returned by this function is in const mode,
   * which prevents any modification of the data.
   *
   * This function is only available if the Mutex type used in the class template is
   * one of:
   *
   * * `std::shared_mutex <https://en.cppreference.com/w/cpp/thread/shared_mutex>`_
   * * `std::shared_timed_mutex <https://en.cppreference.com/w/cpp/thread/shared_timed_mutex>`_
   * @endrst
   */
  template<typename Dummy = void, typename = IsSharedMutex<Mutex, Dummy>>
  auto lock_shared() noexcept -> optional<SyncValGuard<SharedLock<Mutex>, Type, true>>
  {
    try {
      return optional<SyncValGuard<SharedLock<Mutex>, Type, true>>(
          in_place, SharedLock<Mutex>(mutex_val), &value);
    }
    catch (...) {
      // Don't need to unlock the mutex, as we can only end up here from the shared_lock constructor
      // call. If that fails, then the mutex is guaranteed to not be locked (by us).
      // We know that the construction of the optional is noexcept, because optional's constructor
      // is noexcept if the type it wraps has a noexcept constructor, which SyncValGuard does.
    }
    return nullopt;
  }

  /**
   * @brief Lock the mutex in read-only mode and return a handle to make the data accessible.
   *
   * @return optional<SyncValGuard<std::shared_lock<Mutex>, Type, true>>
   *
   * @rst
   * Tries to lock the mutex in shared mode. Returns immediately. On successful lock
   * aquisition, returns :ref:`syncguard_label` in constant mode. Otherwise returns
   * ``seo::nullopt``.
   *
   * This function is allowed to fail spuriously and return ``seo::nullopt`` even if
   * the mutex is not currently exclusively locked by another thread.
   *
   * The behavior is undefined if the calling thread has already aquired this mutex
   * in any mode.
   *
   * This function is only available if the Mutex type used in the class template is
   * one of:
   *
   * * `std::shared_mutex <https://en.cppreference.com/w/cpp/thread/shared_mutex>`_
   * * `std::shared_timed_mutex <https://en.cppreference.com/w/cpp/thread/shared_timed_mutex>`_
   * @endrst
   */
  template<typename Dummy = void, typename = IsSharedMutex<Mutex, Dummy>>
  auto try_lock_shared() noexcept -> optional<SyncValGuard<SharedLock<Mutex>, Type, true>>
  {
    // No try/catch necessary, as try_lock won't throw.
    auto guard = SharedLock<Mutex>(mutex_val, std::defer_lock);
    if (guard.try_lock()) {
      return optional<SyncValGuard<SharedLock<Mutex>, Type, true>>(
          in_place, std::move(guard), &value);
    }
    return nullopt;
  }

  /**
   * @brief Lock the mutex in read-only mode and return a handle to make the data accessible.
   *
   * @param timeout_duration The amount of time to spend trying to lock the mutex
   * @return optional<SyncValGuard<std::shared_lock<Mutex>, Type, true>>
   *
   * @rst
   * Tries to lock the mutex in shared mode. Blocks until the specified
   * ``timeout_duration`` has elapsed or the shared lock is aquired, whichever
   * comes first. On successful aquisition, it returns :ref:`syncguard_label` in
   * constant mode, otherwise it returns ``seo::nullopt``.
   *
   * If ``timeout_duration`` is less than or equal to ``timeout_duration.zero()``,
   * the function behaves like :cpp:any:`seo::SyncVal::try_lock_shared()`.
   *
   * The function may block longer than ``timeout_duration`` due to scheduling or
   * resource contention delays.
   *
   * The standard recommends that a steady clock be used to measure the duration,
   * otherwise it will be susceptible to clock adjustments.
   *
   * As with :cpp:any:`seo::SyncVal::try_lock_shared()`, this function is allowed to fail spuriously
   * and return ``seo::nullopt`` even if the mutex was not exclusively locked by any
   * other thread at some point during ``timeout_duration``.
   *
   * If this function is called by a thread that has already aquired the mutex in any
   * mode, the behavior is undefined.
   *
   * This function is only available if the Mutex type used in the class template is
   * `std::shared_timed_mutex <https://en.cppreference.com/w/cpp/thread/shared_timed_mutex>`_.
   * @endrst
   */
  template<typename Rep,
           typename Period,
           typename Dummy = void,
           typename       = IsSharedMutex<Mutex, Dummy>,
           typename       = IsTimedMutex<Mutex, Dummy>>
  auto try_lock_shared_for(const std::chrono::duration<Rep, Period>& timeout_duration) noexcept
      -> optional<SyncValGuard<SharedLock<Mutex>, Type, true>>
  {
    auto guard = SharedLock<Mutex>(mutex_val, std::defer_lock);
    // Need the try/catch, as if we are given a non-std clock, that clock may throw. The
    // std clock will never throw.
    try {
      if (guard.try_lock_for(timeout_duration)) {
        return optional<SyncValGuard<SharedLock<Mutex>, Type, true>>(
            in_place, std::move(guard), &value);
      }
    }
    catch (...) {
      // Don't need to unlock the mutex, as we can only end up here from the
      // guard.try_lock_shared_for() call. If that fails, then the mutex is guaranteed to not be
      // locked (by us). We know that the construction of the optional is noexcept, because
      // optional's constructor is noexcept if the type it wraps has a noexcept constructor, which
      // SyncValGuard does.
    }
    return nullopt;
  }

  /**
   * @brief Lock the mutex in read-only mode and return a handle to make the data accessible.
   *
   * @param timeout_time The point in time at which to stop trying to lock the mutex
   * @return optional<SyncValGuard<std::shared_lock<Mutex>, Type, true>>
   *
   * @rst
   * Tries to lock the mutex in shared mode. Blocks until specified ``timeout_time``
   * has been reached or the lock aquired, whichever comes first. On successful
   * aquisition, returns :ref:`syncguard_label` in const mode, otherwise returns
   * ``seo::nullopt``.
   *
   * If ``timeout_time`` has already passed, this function behaves like
   * :cpp:any:`seo::SyncVal::try_lock_shared()`.
   *
   * The clock tied to ``timeout_time`` is used, which means that adjustements of
   * the clock are taken into account. Thus the maximum duration of the block might,
   * but might not, be more or less than ``timeout_time - Clock::now()`` at the time
   * of the call, depending on the duration of the adjustment. Additionally, the
   * function may block until after ``timeout_time`` due to scheduling or resource
   * contention delays.
   *
   * As with :cpp:any:`seo::SyncVal::try_lock()`, this function is allowed to fail spuriously and
   * return ``seo::nullopt`` even if the mutex was unlocked at some point before
   * ``timeout_time``.
   *
   * If the calling thread has already locked the mutex, the behavior is undefined.
   *
   * This function is only available if the Mutex type used in the class template is
   * `std::shared_timed_mutex <https://en.cppreference.com/w/cpp/thread/shared_timed_mutex>`_.
   * @endrst
   */
  template<typename Clock,
           typename Duration,
           typename Dummy = void,
           typename       = IsSharedMutex<Mutex, Dummy>,
           typename       = IsTimedMutex<Mutex, Dummy>>
  auto try_lock_shared_until(const std::chrono::time_point<Clock, Duration>& timeout_time) noexcept
      -> optional<SyncValGuard<SharedLock<Mutex>, Type, true>>
  {
    auto guard = SharedLock<Mutex>(mutex_val, std::defer_lock);
    // Need the try/catch, as if we are given a non-std clock, that clock may throw. The
    // std clock will never throw.
    try {
      if (guard.try_lock_until(timeout_time)) {
        return optional<SyncValGuard<SharedLock<Mutex>, Type, true>>(
            in_place, std::move(guard), &value);
      }
    }
    catch (...) {
      // Don't need to unlock the mutex, as we can only end up here from the
      // guard.try_lock_shared_until() call. If that fails, then the mutex is guaranteed to not be
      // locked (by us). We know that the construction of the optional is noexcept, because
      // optional's constructor is noexcept if the type it wraps has a noexcept constructor, which
      // SyncValGuard does.
    }
    return nullopt;
  }
};

/**
 * @brief Provide access to data inside of a seo::SyncVal class
 *
 * @tparam Guard The type of mutex guard the class contains
 * @tparam Type The type of data that the class refers to
 * @tparam CONST Whether the class is allowed to mutate the data it refers to
 *
 * @rst
 * This is a helper class that controls the lifetime of the mutex lock, as well as the
 * access to the data within the :ref:`syncval_label`. The user should never directly
 * instantiate this class, instead it is returned by the various ``lock()`` functions
 * of :ref:`syncval_label`.
 *
 * While this class remains instantiated, the mutex within :ref:`syncval_label` remains
 * locked. The destructor of this class will unlock the mutex. Additionally, this class
 * acts like a pointer to the data within :ref:`syncval_label`, providing
 * the standard pointer members, such as ``operator*()``, ``operator->()``, and ``get()``.
 *
 * As implied earlier, the lifetime of the class instantiation is the lifetime of the data access
 * *and* the mutex lock. As long as there are no reference/pointer escapes, there can
 * be no data race, and the mutex will automatically unlock once you no longer need
 * access to the data (provided you scope the instantiation of this class properly).
 *
 * Additionally, the various ``std::condition_variable`` functions are supported on this class,
 * if a condition variable is supplied. This allows waiting on a condition variable with the wrapped
 * data.
 *
 * This class is not copyable, but it is movable for ergonomic reasons.
 *
 * .. warning::
 *
 *  Do not, under *any* circumstances, use a moved-from :ref:`syncguard_label`, except for
 *  assignment-to. Any other usage will result in undefined behavior.
 *
 * This class acts as a pointer-type, which points to the protected value.
 *
 * The standard use of this class is shown in the following example::
 *
 *  #include <seo_sync_val.hpp>
 *  #include <thread>
 *  #include <mutex>
 *  #include <string>
 *  #include <memory>
 *  #include <iostream>
 *
 *  int main() {
 *      auto shared_val = std::make_shared<seo::SyncVal<std::mutex, std::string>>();
 *      auto t1 = std::thread{[=](){
 *          // locked_val is a seo::optional<seo::SyncValGuard>
 *          for (unsigned int i = 0; i < 2; i++) {
 *              auto locked_val = shared_val->lock();
 *              if (i == 0) {
 *                  locked_val.get() = "Hello World!";
 *              }
 *              else {
 *                  // Or we could do this, just don't ever use locked_val afterwards
 *                  auto val = std::move(locked_val);
 *                  val.get() = "Hello World 2.0";
 *              } // val goes out of scope here, so the mutex is released here
 *              // when the else block is executed
 *
 *          } // locked_val goes out of scope here, releasing the mutex if i == 0.
 *      }};
 *      t1.join();
 *
 *      // Can also use dereference operator.
 *      // Prints Hello World 2.0
 *      std::cout << *(shared_val->lock()) << "\n";
 *      return 0;
 *  }
 *
 * .. warning::
 *
 *  Do not, under any circumstances, try to save the reference returned
 *  by the accessor functions/operators of :ref:`syncguard_label`. If you manage to
 *  retain a reference after :ref:`syncguard_label` goes out of scope, then you will
 *  be able to access the data without locking the mutex. This will lead to data races.
 *
 * All members of this class are marked ``const``, as they do not mutate they class
 * itself. Additionally, the version of this class returned by any variant of the
 * :ref:`syncval_label`'s ``lock_shared()`` functions will not allow mutation of the
 * data. All references returned by the accessors are constant. This is to uphold the
 * invariant that a shared lock should only read the data, not mutate it.
 *
 * @endrst
 */
template<typename Guard, typename Type, bool CONST>
// Disable check, as we do, in fact define the copy assignment, but the SFINAE confuses.
// NOLINTNEXTLINE(hicpp-special-member-functions,cppcoreguidelines-special-member-functions)
class SyncValGuard final {
private:
  Guard my_mutex_guard;
  Type* my_val_ref;

public:
  ~SyncValGuard() = default;  // No need for anything special, it's taken care of by SyncValStorage

  // Make public to allow tl::optional or std::optional to wrap. Don't add to
  // doxygen, as it should never be used by the user.
  explicit constexpr SyncValGuard(Guard&& mutex_param, Type* val_param) noexcept :
      my_mutex_guard(std::forward<Guard>(mutex_param)), my_val_ref(val_param)
  {
  }

/**
 * @brief Construct a new Sync Val Guard object
 *
 * @param old The SyncValGuard being moved from.
 *
 * @rst
 * This enables move construction. The primary purpose of this is to allow
 * extracting the :ref:`syncguard_label` from the wrapping optional if desired.
 *
 * It also allows this class to be returned from the :ref:`syncval_label`
 * ``lock`` functions prior to C++17's guaranteed copy elision.
 *
 * .. warning::
 *
 *  Never, under any circumstances, should the moved-from :ref:`syncguard_label`
 *  be used, except for move-assignment-to. Any other use will result in undefined behavior.
 *
 * @endrst
 */
#if __cplusplus >= 201402L
  constexpr
#endif
      SyncValGuard(SyncValGuard&& old) noexcept(std::is_nothrow_move_constructible<Type&>::value): my_mutex_guard(std::move(old.my_mutex_guard)), my_val_ref (std::move(old.my_val_ref)) {
        old.my_val_ref = nullptr;
      }

/**
 * @brief Move Assignment
 *
 * @param old The value to move from
 * @return A reference to self
 *
 * @rst
 * Move assignment. This operates as if :ref:`syncguard_label` were a pointer. I.E. it moves the
 * mutex guard from the left into the right, and then moves the *pointer* from the left into the
 * right. (Not the value wrapped by :ref:`syncguard_label`).
 *
 * The result is that the :ref:`syncguard_label` on the left now gives access to and controls the
 * lock for, the value that used to be wrapped by the right.
 *
 * .. note::
 *
 *  This means that if you want to move a value into the wrapped value, you have to use accessor
 *  functions, such as :cpp:any:`seo::SyncValGuard::get()`.
 *
 * .. warning::
 *
 *  Do not perform any operations on a moved-from :ref:`syncguard_label` other than this operation.
 *  Any other operation will result in undefined behavior.
 *
 * **Example**::
 *
 *  seo::SyncVal<std::mutex, std::string>       synced_string{""};
 *  seo::SyncVal<std::timed_mutex, std::string> synced_string2{"Hello"};
 *  seo::SyncVal<std::mutex, std::string>       synced_string3{"Hello2"};
 *
 *  // Copies wrapped value "Hello" from synced_string2 into synced_string.
 *  // This is because of implicit conversion.
 *  synced_string.lock().get() = synced_string2.lock().get();
 *
 *  // Copies wrapped value "Hello2" from synced_string3 into synced_string.
 *  {
 *    auto locked_val = synced_string3.lock();
 *    synced_string.lock().get() = locked_val.get();
 *  }
 *
 *  // Does nothing. This will create an r-value of SyncValGuard that gives access to
 *  // synced_string. It then moves the lock and pointer from synced_string3 into it, so that
 *  // the r-value now gives access to synced_string3. It then immediately goes out of scope
 *  // releasing the lock.
 *  synced_string.lock() = synced_string3.lock();
 *
 *  {
 *    // Gets the SyncValGaurd out of the optional and saves it to locked_val
 *    // locked_val currently controls access to the value wrapped by synced_string
 *    auto locked_val = synced_string.lock();
 *
 *    // Copy "Word!" into value wrapped by synced_string
 *    *locked_val = "Word!";
 *
 *    // Move the SyncValGuard for synced_string3 into locked_val
 *    // Release the lock on synced_string
 *    // locked_val now controls the lock on synced_string3, and gives access to the value
 *    // inside of synced_string3
 *    locked_val = synced_string3.lock();
 *
 *    // Copy "B" into value wrapped by synced_string3
 *    *locked_val = "B";
 *  }
 *
 * @endrst
 *
 */
#if __cplusplus >= 201402L
  constexpr
#endif
      auto
      operator=(SyncValGuard&& old) noexcept -> SyncValGuard& {
        my_val_ref = std::move(old.my_val_ref);
        my_mutex_guard = std::move(old.my_mutex_guard);
        old.my_val_ref = nullptr;
        return *this;
      };

  // Disable Copy Construction
  SyncValGuard(SyncValGuard const& other) = delete;

  // Disable copy assignment
  auto operator=(SyncValGuard const& rhs) -> SyncValGuard& = delete;

  /**
   * @brief Access to Type
   *
   * @return Type*
   *
   * @rst
   * Dereference and automatically get member of Type.
   *
   * **Example**::
   *
   *  #include <seo_sync_val.hpp>
   *  #include <memory>
   *
   *  struct SomeStruct {
   *      int a;
   *      char b;
   *  };
   *
   *  int main() {
   *      auto synced_val = std::make_shared<seo::SyncVal<std::mutex, SomeStruct>>();
   *
   *      auto locked_val = synced_val->lock();
   *      locked_val->a = 100;
   *      locked_val->b = '0';
   *      
   *      return 0;
   *  }
   *
   * @endrst
   */
  template<typename Dummy = void, typename = typename std::enable_if<!CONST, Dummy>::type>
  constexpr auto operator->() const noexcept -> Type*
  {
    assert(my_mutex_guard.owns_lock());
    return my_val_ref;
  }

  template<typename Dummy = void, typename = typename std::enable_if<CONST, Dummy>::type>
  constexpr auto operator->() const noexcept -> Type const*
  {
    assert(my_mutex_guard.owns_lock());
    return my_val_ref;
  }

  /**
   * @brief Dereference to Type
   *
   * @return Type&
   *
   * @rst
   * **Example**::
   *
   *  #include <seo_sync_val.hpp>
   *  #include <memory>
   *
   *  int main() {
   *      auto synced_val = std::make_shared<seo::SyncVal<std::mutex, int>>();
   *
   *      {
   *          auto locked_val = synced_val->lock();
   *
   *          *locked_val = 100;
   *      }
   *      return 0;
   *  }
   *
   * @endrst
   */
  template<typename Dummy = void, typename = typename std::enable_if<!CONST, Dummy>::type>
  constexpr auto operator*() const noexcept -> Type&
  {
    assert(my_mutex_guard.owns_lock());
    return *my_val_ref;
  }

  template<typename Dummy = void, typename = typename std::enable_if<CONST, Dummy>::type>
  constexpr auto operator*() const noexcept -> Type const&
  {
    assert(my_mutex_guard.owns_lock());
    return *my_val_ref;
  }

  /**
   * @brief Explicit retrieval of Type
   *
   * @return Type&
   *
   * @rst
   * **Example**::
   *
   *  #include <seo_sync_val.hpp>
   *  #include <memory>
   *
   *  int main() {
   *      auto synced_val = std::make_shared<seo::SyncVal<std::mutex, int>>();
   *
   *      {
   *          auto locked_val = synced_val->lock();
   *
   *          locked_val.get() = 100;
   *      }
   *      return 0;
   *  }
   *
   * @endrst
   */
  template<typename Dummy = void, typename = typename std::enable_if<!CONST, Dummy>::type>
  constexpr auto get() const noexcept -> Type&
  {
    assert(my_mutex_guard.owns_lock());
    return *my_val_ref;
  }

  template<typename Dummy = void, typename = typename std::enable_if<CONST, Dummy>::type>
  constexpr auto get() const noexcept -> Type const&
  {
    assert(my_mutex_guard.owns_lock());
    return *my_val_ref;
  }

  /**
   * @brief Wait on Condition Variable
   *
   * @param cv The condition variable to wait on
   *
   * @rst
   * This function takes a ``std::condition_variable`` and then calls the
   * ``std::condition_variable::wait()`` function, using the internal lock of the object and the
   * given ``condition_variable``. This blocks the current thread until the condition variable is
   * notified or a spurious wakeup occurs. It will unlock the mutex prior to the block, and
   * automatically re-aquire it when it wakes up.
   *
   * @endrst
   */
  template<typename Dummy = void, typename = IsCvCompatible<Guard, Dummy>>
  auto wait(std::condition_variable& cv) -> void
  {
    cv.wait(my_mutex_guard);
  }

  /**
   * @brief Wait on Condition Variable with Predicate
   *
   * @param cv Condition Variable to wait on
   * @param p Predicate to give to Condition Variable
   *
   * @rst
   * This function takes a ``std::condition_variable`` and then calls the
   * ``std::condition_variable::wait()`` function, using the internal lock of the object and the
   * given ``condition_variable``. This blocks the current thread until the condition variable is
   * notified or a spurious wakeup occurs. It will unlock the mutex prior to the block, and
   * automatically re-aquire it when it wakes up.
   *
   * This overload may be used to avoid spurious wakeups, as it is equivalent to::
   *
   *  while (!p()) {
   *    wait(cv);
   *  }
   *
   * The predicate ``p`` must be a function with the signature ``bool p()``. It returns false if the
   * condition variable should not awaken the thread.
   *
   * **Example** (modified from the cppreference.com example for ``std::condition_variable``)::
   *
   *  #include <iostream>
   *  #include <string>
   *  #include <thread>
   *  #include <seo_sync_val.hpp>
   *  #include <condition_variable>
   *
   *  struct DataStruct {
   *    std::string data;
   *    bool ready;
   *    bool processed;
   *  };
   *
   *  void worker_thread(seo::SyncVal<std::mutex, DataStruct>& data, std::condition_variable& cv) {
   *    // Wait until main() sends data
   *    { // Scope the lock so that we can signal after the locked_data goes out of scope and
   *      // unlocks mutex.
   *      auto locked_data = data.lock();
   *
   *      locked_data.wait(cv, [&]{return locked_data->ready;});
   *
   *      // after the wait, we own the lock.
   *      std::cout << "Worker thread is processing data\n";
   *      locked_data->data += " after processing";
   *
   *      // Send data back to main()
   *      locked_data->processed = true;
   *
   *      std::cout << "Worker thread signals data processing completed\n";
   *    }
   *    cv.notify_one();
   *  }
   *
   *  int main() {
   *    std::condition_variable cv;
   *    seo::SyncVal<std::mutex, DataStruct> data;
   *
   *    std::thread worker(worker_thread, std::ref(data), std::ref(cv));
   *
   *    // send data to the worker thread
   *    {
   *      auto locked_data = data.lock();
   *      locked_data->data = "Example data";
   *      locked_data->ready = true;
   *      std::cout << "main() signals data ready for processing\n";
   *    }
   *    cv.notify_one();
   *    // wait for the worker
   *    {
   *      auto locked_data = data.lock();
   *      locked_data.wait(cv, [&]{return locked_data->processed;});
   *    }
   *    std::cout << "Back in main(), data = " << data.lock()->data << '\n';
   *    worker.join();
   *  }
   * @endrst
   */
  template<typename Predicate, typename Dummy = void, typename = IsCvCompatible<Guard, Dummy>>
  auto wait(std::condition_variable& cv, Predicate p) -> void
  {
    cv.wait(my_mutex_guard, p);
  }

  /**
   * @brief Wait for Condition Variable Signal with Specified Timeout
   *
   * @param cv Condition Variable to wait on
   * @param rel_time Timeout duration
   * @return std::cv_status::timeout if rel_time expired
   * @return std::cv_status::no_timeout otherwise
   *
   * @rst
   * Atomically releases the lock, blocks the current thread, and adds it to the list of threads
   * waiting on the condition variable. The thread will be unblocked when ``notify_all()`` or
   * ``notify_one()`` is executed, or when the relative timeout ``rel_time`` expires. It may also
   * unblock spuriously. When unblocked, the lock is re-aquired and the function exits. A steady
   * clock should be used to measure the duration. The function may block longer than ``rel_time``
   * due to scheduling or resource contention delays.
   * @endrst
   */
  template<typename Rep, typename Period, typename = IsCvCompatible<Guard, Rep>>
  auto wait_for(std::condition_variable& cv, std::chrono::duration<Rep, Period> const& rel_time)
      -> std::cv_status
  {
    return cv.wait_for(my_mutex_guard, rel_time);
  }

  /**
   * @brief Wait on Condition Variable with Specified Timeout
   *
   * @param cv Condition Variable to wait on
   * @param rel_time Timeout duration
   * @param pred Predicate to avoid spurious wakeups. While returns false, the thread won't wake up
   * @return true If predicate returns true and ``cv`` is signaled prior to ``rel_time``
   * @return false If predicate ``pred`` still evaluates to false after ``rel_time`` expires
   *
   * @rst
   * Atomically releases the lock, blocks the current thread, and adds it to the list of threads
   * waiting on the condition variable. The thread will be unblocked when ``notify_all()`` or
   * ``notify_one()`` is executed, or when the relative timeout ``rel_time`` expires.
   * When unblocked, the lock is re-aquired and the function exits. A steady
   * clock should be used to measure the duration. The function may block longer than ``rel_time``
   * due to scheduling or resource contention delays.
   *
   * This overload avoids spurious wakeups. Equivalent to::
   *
   *  return wait_until(cv, std::chrono::steady_clock::now() + rel_time, std::move(pred));
   *
   * The predicate ``pred`` must be a function with the signature ``bool pred()``. It returns false
   * if the condition variable should not awaken the thread.
   * @endrst
   */
  template<typename Rep, typename Period, typename Predicate, typename = IsCvCompatible<Guard, Rep>>
  auto wait_for(std::condition_variable&                  cv,
                std::chrono::duration<Rep, Period> const& rel_time,
                Predicate                                 pred) -> bool
  {
    return cv.wait_for(my_mutex_guard, rel_time, pred);
  }

  /**
   * @brief Wait on Condition Variable Until Timeout Time
   *
   * @param cv Conditon Variable to wait on
   * @param timeout_time Time to timeout on
   * @return std::cv_status::timeout If ``timeout_time`` reached
   * @return std::cv_status::no_timeout Otherwise
   *
   * @rst
   * Cause current thread to block until condition variable is notified, a specific time reached,
   * or a spurious wakeup occurs. The lock is released before blocking occurs and is reaquired when
   * the thread wakes up.
   * @endrst
   */
  template<typename Clock, typename Duration, typename = IsCvCompatible<Guard, Clock>>
  auto wait_until(std::condition_variable&                        cv,
                  std::chrono::time_point<Clock, Duration> const& timeout_time) -> std::cv_status
  {
    return cv.wait_until(my_mutex_guard, timeout_time);
  }

  /**
   * @brief Wait on Conditon Variable Until Timeout Time
   *
   * @param cv Condition Variable to wait on
   * @param timeout_time The time to timeout the wait on
   * @param pred Functor that returns false if the thread shouldn't wake up
   * @return true If predicate returns true and ``cv`` is signaled prior to ``timeout_time``
   * @return false If predicate ``pred`` still evaluates to false after ``timeout_time`` expires
   *
   * @rst
   * Cause current thread to block until condition variable is notified or a specific time reached.
   * The lock is released before blocking occurs and is reaquired when the thread wakes up.
   *
   * This overload avoids spurious wakeups. Equivalent to::
   *
   *  while(!pred()) {
   *    if (wait_until(cv, timeout_time) == std::cv_status::timeout) {
   *      return pred();
   *    }
   *  }
   *  return true;
   *
   * The predicate ``pred`` must be a function with the signature ``bool pred()``. It returns false
   * if the condition variable should not awaken the thread.
   * @endrst
   */
  template<typename Clock,
           typename Duration,
           typename Predicate,
           typename = IsCvCompatible<Guard, Clock>>
  auto wait_until(std::condition_variable&                        cv,
                  std::chrono::time_point<Clock, Duration> const& timeout_time,
                  Predicate                                       pred) -> bool
  {
    return cv.wait_until(my_mutex_guard, timeout_time, pred);
  }

  /**
   * @brief Wait on Condition Variable
   *
   * @param cv Condition Variable to wait on.
   *
   * @rst
   * Overloads :cpp:any:`seo::SyncValGuard::wait()` for use with mutexes other than ``std::mutex``.
   * @endrst
   */
  auto wait(std::condition_variable_any& cv) -> void
  {
    cv.wait(my_mutex_guard);
  }

  /**
   * @brief Wait on Condition Variable
   *
   * @param cv Condition Variable to wait on
   * @param p Predicate. Wait will continue while it returns true
   *
   * @rst
   * Overloads :cpp:any:`seo::SyncValGuard::wait()` for use with mutexes other than ``std::mutex``.
   * @endrst
   */
  template<typename Predicate>
  auto wait(std::condition_variable_any& cv, Predicate p) -> void
  {
    cv.wait(my_mutex_guard, p);
  }

  /**
   * @brief Wait on Conditon Variable with Timeout
   *
   * @param cv Condition variable to wait on
   * @param rel_time Timeout duration
   * @return std::cv_status::timeout If ``rel_time`` passed
   * @return std::cv_status::no_timeout Otherwise
   *
   * @rst
   * Overloads :cpp:any:`seo::SyncValGuard::wait_for()` for use with mutexes other than
   * ``std::mutex``.
   * @endrst
   */
  template<typename Rep, typename Period>
  auto wait_for(std::condition_variable_any& cv, std::chrono::duration<Rep, Period> const& rel_time)
      -> std::cv_status
  {
    return cv.wait_for(my_mutex_guard, rel_time);
  }

  /**
   * @brief Wait on Condition Variable with Specified Timeout
   *
   * @param cv Condition Variable to wait on
   * @param rel_time Timeout duration
   * @param pred Predicate to avoid spurious wakeups. While returns false, the thread won't wake up
   * @return true If predicate returns true and ``cv`` is signaled prior to ``rel_time``
   * @return false If predicate ``pred`` still evaluates to false after ``rel_time`` expires
   *
   * @rst
   * Overloads :cpp:any:`seo::SyncValGuard::wait_for()` for use with mutexes other than
   * ``std::mutex``.
   * @endrst
   */
  template<typename Rep, typename Period, typename Predicate>
  auto wait_for(std::condition_variable_any&              cv,
                std::chrono::duration<Rep, Period> const& rel_time,
                Predicate                                 pred) -> bool
  {
    return cv.wait_for(my_mutex_guard, rel_time, pred);
  }

  /**
   * @brief Wait on Condition Variable Until Timeout Time
   *
   * @param cv Conditon Variable to wait on
   * @param timeout_time Time to timeout on
   * @return std::cv_status::timeout If ``timeout_time`` reached
   * @return std::cv_status::no_timeout Otherwise
   *
   * @rst
   * Overloads :cpp:any:`seo::SyncValGuard::wait_until()` for use with mutexes other than
   * ``std::mutex``.
   * @endrst
   */
  template<typename Clock, typename Duration>
  auto wait_until(std::condition_variable_any&                    cv,
                  std::chrono::time_point<Clock, Duration> const& timeout_time) -> std::cv_status
  {
    return cv.wait_until(my_mutex_guard, timeout_time);
  }

  /**
   * @brief Wait on Conditon Variable Until Timeout Time
   *
   * @param cv Condition Variable to wait on
   * @param timeout_time The time to timeout the wait on
   * @param pred Functor that returns false if the thread shouldn't wake up
   * @return true If predicate returns true and ``cv`` is signaled prior to ``timeout_time``
   * @return false If predicate ``pred`` still evaluates to false after ``timeout_time`` expires
   *
   * @rst
   * Overloads :cpp:any:`seo::SyncValGuard::wait_until()` for use with mutexes other than
   * ``std::mutex``.
   * @endrst
   */
  template<typename Clock, typename Duration, typename Predicate>
  auto wait_until(std::condition_variable_any&                    cv,
                  std::chrono::time_point<Clock, Duration> const& timeout_time,
                  Predicate                                       pred) -> bool
  {
    return cv.wait_until(my_mutex_guard, timeout_time, pred);
  }
};

/**
 * @brief Lock two SyncVal's without Deadlock
 *
 * @param synced_val1 A SyncVal to lock
 * @param synced_val2 Another SyncVal to lock
 * @return std::pair<optional<SyncValGuard<M1, T1, false>>, optional<SyncValGuard<M2, T2, false>>>
 * A pair containing the return values of the lock() call on synced_val1 and synced_val2, in the
 * same order as they were passed into the function.
 *
 * @rst
 * This function will lock the two :ref:`syncval_label`s in the same order, regardless of what order
 * the parameters are passed in. The object with a larger memory address is locked first, followed
 * by the second. Regardless of lock order, the :ref:`syncguard_label`s are returned in the same
 * order as the :ref:`syncval_label`s are passed in. I.E. the first parameter will be the first
 * in the std::pair, and the second will be the second.
 *
 * The :ref:`syncval_label` are locked using the :cpp:any:`seo::SyncVal::lock()` function.
 *
 * .. note::
 *
 *  There is an overload of this function that takes three :ref:`syncguard_label`s and returns
 *  a std::tuple of :ref:`syncguard_label`s.
 * @endrst
 */
template<typename M1, typename T1, typename M2, typename T2>
auto lock(SyncVal<M1, T1>& synced_val1, SyncVal<M2, T2>& synced_val2)
    -> std::pair<SyncValGuard<std::unique_lock<M1>, T1, false>,
                 SyncValGuard<std::unique_lock<M2>, T2, false>>
{
  if (static_cast<void*>(&synced_val1) > static_cast<void*>(&synced_val2)) {
    auto locked1 = synced_val1.lock();
    auto locked2 = synced_val2.lock();
    return std::make_pair(std::move(locked1), std::move(locked2));
  }

  auto locked2 = synced_val2.lock();
  auto locked1 = synced_val1.lock();
  return std::make_pair(std::move(locked1), std::move(locked2));
}

template<typename M1, typename T1, typename M2, typename T2, typename M3, typename T3>
auto lock(SyncVal<M1, T1>& synced_val1, SyncVal<M2, T2>& synced_val2, SyncVal<M3, T3>& synced_val3)
    -> std::tuple<SyncValGuard<std::unique_lock<M1>, T1, false>,
                  SyncValGuard<std::unique_lock<M2>, T2, false>,
                  SyncValGuard<std::unique_lock<M3>, T3, false>>
{
  void const* const addr1 = &synced_val1;
  void const* const addr2 = &synced_val2;
  void const* const addr3 = &synced_val3;

  // 1 < 2 < 3
  if (addr1 < addr2 && addr2 < addr3) {
    auto locked3 = synced_val3.lock();
    auto locked2 = synced_val2.lock();
    auto locked1 = synced_val1.lock();
    return std::make_tuple(std::move(locked1), std::move(locked2), std::move(locked3));
  }
  // 1 < 3 < 2
  if (addr1 < addr3 && addr3 < addr2) {
    auto locked2 = synced_val2.lock();
    auto locked3 = synced_val3.lock();
    auto locked1 = synced_val1.lock();
    return std::make_tuple(std::move(locked1), std::move(locked2), std::move(locked3));
  }
  // 3 < 1 < 2
  if (addr3 < addr1 && addr1 < addr2) {
    auto locked2 = synced_val2.lock();
    auto locked1 = synced_val1.lock();
    auto locked3 = synced_val3.lock();
    return std::make_tuple(std::move(locked1), std::move(locked2), std::move(locked3));
  }
  // 2 < 1 < 3
  if (addr2 < addr1 && addr1 < addr3) {
    auto locked3 = synced_val3.lock();
    auto locked1 = synced_val1.lock();
    auto locked2 = synced_val2.lock();
    return std::make_tuple(std::move(locked1), std::move(locked2), std::move(locked3));
  }
  // 2 < 3 < 1
  if (addr2 < addr3 && addr3 < addr1) {
    auto locked1 = synced_val1.lock();
    auto locked3 = synced_val3.lock();
    auto locked2 = synced_val2.lock();
    return std::make_tuple(std::move(locked1), std::move(locked2), std::move(locked3));
  }
  // 3 < 2 < 1
  auto locked1 = synced_val1.lock();
  auto locked2 = synced_val2.lock();
  auto locked3 = synced_val3.lock();
  return std::make_tuple(std::move(locked1), std::move(locked2), std::move(locked3));
}

/**
 * @brief Try_Lock two SyncVal's without Deadlock
 *
 * @param synced_val1 A SyncVal to lock
 * @param synced_val2 Another SyncVal to lock
 * @return std::pair<optional<SyncValGuard<M1, T1, false>>, optional<SyncValGuard<M2, T2, false>>>
 * A pair containing the return values of the lock() call on synced_val1 and synced_val2, in the
 * same order as they were passed into the function.
 *
 * @rst
 * This function will attempt lock the two :ref:`syncval_label`s in the same order, regardless of
 * what order the parameters are passed in. The object with a larger memory address is locked first,
 * followed by the second. Regardless of lock order, the :ref:`syncguard_label`s are returned in the
 * same order as the :ref:`syncval_label`s are passed in. I.E. the first parameter will be the first
 * in the std::pair, and the second will be the second.
 *
 * The :ref:`syncval_label` are locked using the :cpp:any:`seo::SyncVal::try_lock()` function.
 *
 * .. note::
 *
 *  There is an overload of this function that takes three :ref:`syncval_label`s and returns a
 *  std::tuple of :ref:`syncguard_label`s.
 * @endrst
 */
template<typename M1, typename T1, typename M2, typename T2>
auto try_lock(SyncVal<M1, T1>& synced_val1, SyncVal<M2, T2>& synced_val2) noexcept
    -> std::pair<optional<SyncValGuard<std::unique_lock<M1>, T1, false>>,
                 optional<SyncValGuard<std::unique_lock<M2>, T2, false>>>
{
  if (static_cast<void*>(&synced_val1) > static_cast<void*>(&synced_val2)) {
    auto locked1 = synced_val1.try_lock();
    auto locked2 = synced_val2.try_lock();

    if (!locked1.has_value() || !locked2.has_value()) {
      return std::make_pair(nullopt, nullopt);
    }
    return std::make_pair(std::move(locked1), std::move(locked2));
  }

  auto locked2 = synced_val2.try_lock();
  auto locked1 = synced_val1.try_lock();
  if (!locked1.has_value() || !locked2.has_value()) {
    return std::make_pair(nullopt, nullopt);
  }
  return std::make_pair(std::move(locked1), std::move(locked2));
}

template<typename M1, typename T1, typename M2, typename T2, typename M3, typename T3>
auto try_lock(SyncVal<M1, T1>& synced_val1,
              SyncVal<M2, T2>& synced_val2,
              SyncVal<M3, T3>& synced_val3) noexcept
    -> std::tuple<optional<SyncValGuard<std::unique_lock<M1>, T1, false>>,
                  optional<SyncValGuard<std::unique_lock<M2>, T2, false>>,
                  optional<SyncValGuard<std::unique_lock<M3>, T3, false>>>
{
  void const* const addr1 = &synced_val1;
  void const* const addr2 = &synced_val2;
  void const* const addr3 = &synced_val3;

  // 1 < 2 < 3
  if (addr1 < addr2 && addr2 < addr3) {
    auto locked3 = synced_val3.try_lock();
    auto locked2 = synced_val2.try_lock();
    auto locked1 = synced_val1.try_lock();

    if (!locked1.has_value() || !locked2.has_value() || !locked3.has_value()) {
      return std::make_tuple(nullopt, nullopt, nullopt);
    }
    return std::make_tuple(std::move(locked1), std::move(locked2), std::move(locked3));
  }

  // 1 < 3 < 2
  if (addr1 < addr3 && addr3 < addr2) {
    auto locked2 = synced_val2.try_lock();
    auto locked3 = synced_val3.try_lock();
    auto locked1 = synced_val1.try_lock();

    if (!locked1.has_value() || !locked2.has_value() || !locked3.has_value()) {
      return std::make_tuple(nullopt, nullopt, nullopt);
    }
    return std::make_tuple(std::move(locked1), std::move(locked2), std::move(locked3));
  }

  // 3 < 1 < 2
  if (addr3 < addr1 && addr1 < addr2) {
    auto locked2 = synced_val2.try_lock();
    auto locked1 = synced_val1.try_lock();
    auto locked3 = synced_val3.try_lock();

    if (!locked1.has_value() || !locked2.has_value() || !locked3.has_value()) {
      return std::make_tuple(nullopt, nullopt, nullopt);
    }
    return std::make_tuple(std::move(locked1), std::move(locked2), std::move(locked3));
  }

  // 2 < 1 < 3
  if (addr2 < addr1 && addr1 < addr3) {
    auto locked3 = synced_val3.try_lock();
    auto locked1 = synced_val1.try_lock();
    auto locked2 = synced_val2.try_lock();

    if (!locked1.has_value() || !locked2.has_value() || !locked3.has_value()) {
      return std::make_tuple(nullopt, nullopt, nullopt);
    }
    return std::make_tuple(std::move(locked1), std::move(locked2), std::move(locked3));
  }

  // 2 < 3 < 1
  if (addr2 < addr3 && addr3 < addr1) {
    auto locked1 = synced_val1.try_lock();
    auto locked3 = synced_val3.try_lock();
    auto locked2 = synced_val2.try_lock();

    if (!locked1.has_value() || !locked2.has_value() || !locked3.has_value()) {
      return std::make_tuple(nullopt, nullopt, nullopt);
    }
    return std::make_tuple(std::move(locked1), std::move(locked2), std::move(locked3));
  }

  // 3 < 2 < 1
  auto locked1 = synced_val1.try_lock();
  auto locked2 = synced_val2.try_lock();
  auto locked3 = synced_val3.try_lock();

  if (!locked1.has_value() || !locked2.has_value() || !locked3.has_value()) {
    return std::make_tuple(nullopt, nullopt, nullopt);
  }
  return std::make_tuple(std::move(locked1), std::move(locked2), std::move(locked3));
}

}  // namespace seo